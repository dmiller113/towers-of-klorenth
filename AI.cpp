//Towers of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//AI.h

//Includes
#include "AI.h"
#include "libtcod.hpp"
#include "messageQueue.h"
#include "gameEngine.h"
#include "graphics.h"
#include <cmath>

//Defines


//Globals
std::string helpMessage = "You've found yourself in a mysterious tower after falling asleep last night.  The air is charged with a strange energy, and the very walls crawl under your gaze. In the distance you hear a faint buzzing sound. Suddenly, a scream breaks the silence, and a sinister voice shouts out \"Klorenth sees you mortal. I see you! Klorenth comes for you!\" You wonder if you can survive this night...\n\n\nControls:\n\nKeypad or Arrows: Movement, attack\ni: Examine an inventory item\nx: Examine a map tile\nw: Wear an item\nr: Remove a worn item\na: Use or Apply an item\nz: Use or Zap a spell/ability\n@: Display status sheet.\nc: Close a door.";

//Enums


//Classes


//Functions

/*
AI::AI()
{
    //ctor
}
*/

AI::~AI()
{
    //dtor
}

bool AI::changeStamina( int cValue )
{
    //stamina caps at 100. Make sure this doesn't put that above the max.
    if( (_stamina + cValue) > 100)
    {
        _stamina = 100;
        return true;
    }
    else if( (_stamina + cValue) < 0)   //check to make sure that this doesn't put stamina below 0
    {
        return false;
    }else
    {
        _stamina += cValue;
        return true;
    }
}

float AI::calcLineDistance(float x1, float y1, float x2, float y2)
{
    float answer = 0;
    float dX = (x2 - x1);
    float dY = (y2 - y1);
    answer = dX * dX + dY * dY;
    answer = sqrtf(answer);
    return answer;
}

bool AI::changeEnergy(int cValue )
{
    //change energy value. This is the time piece for turns.
    if( (_energy + cValue) < 0)
    {
        return false;
    }else if( (_energy + cValue) > 100)
    {
        _energy = 100;
        return true;
    }else
    {
        _energy += cValue;
        return true;
    }
}

bool AI::energyTick( void )
{
    //Responsable for regenerating energy for the AI object. Returns true when energy == 100 and the object can act.
    changeEnergy(_energyRecharge);
    if(_energy == 100)
        return true;
    return false;
}

void playerAI::update(object* owner, gameState& gs, map* currentMap)
{
    //dis is the player, handle input here.
    TCOD_key_t key = TCODConsole::checkForKeypress(TCOD_KEY_PRESSED);//handling input;
    int cX = 0;
    int cY = 0;
    int tempPPos[2];
    int tempOPos[2];
    object* tempObject = NULL;
    bool pickedUp = false;
    gs = GAMEIDLE;

    if(_stamina < 100)
    {
        changeStamina(5);
    }

    if(key.c == 'Q')
    {
        gs = GAMEOVER;
        return;
    }
    if(!owner->getDestroyable()->isDead() && _energy == 100)
    {
        if(owner->getTraits() && owner->getTraits()->_onUpkeep)
        {
            owner->getTraits()->_onUpkeep->apply->applyTo(owner, owner);
        }
        gs = GAMEPLAYERACT;
        if(key.c == '4' || key.vk == TCODK_LEFT)
        {
            cX = -1;
            cY = 0;
            gs = GAMEMOVED;
        }
        if(key.c == '6' || key.vk == TCODK_RIGHT)
        {
            cX = 1;
            cY = 0;
            gs = GAMEMOVED;
        }
        if(key.c == '8' || key.vk == TCODK_UP)
        {
            cX = 0;
            cY = -1;
            gs = GAMEMOVED;
        }
        if(key.c == '2' || key.vk == TCODK_DOWN)
        {
            cX = 0;
            cY = 1;
            gs = GAMEMOVED;
        }
        if(key.c == '7')
        {
            cX = -1;
            cY = -1;
            gs = GAMEMOVED;
        }
        if(key.c == '9')
        {
            cX = 1;
            cY = -1;
            gs = GAMEMOVED;
        }
        if(key.c == '1')
        {
            cX = -1;
            cY = 1;
            gs = GAMEMOVED;
        }
        if(key.c == '3')
        {
            cX = 1;
            cY = 1;
            gs = GAMEMOVED;
        }
        if(key.c == '>')
        {
            owner->getPos(tempPPos);
            if(true || game->getMap()->getTile(tempPPos[0], tempPPos[1])->_symbol == '>')
            {
                gs = GAMESTAIR;
                game->nextLevel();
                messager.addMessage("You descend down the stairs...", TCODColor::white);
                messager.addMessage("The stairs behind you disapear into grey fog.", TCODColor::white);
            }else
            {
                messager.addMessage("There are no stairs here to climb down...", TCODColor::brass);
            }
        }
        if(key.c == '5')
        {
            gs = GAMEIDLE;
            owner->getAI()->changeEnergy(20);
        }
        if(key.c == 'i')
        {
            std::vector<std::string> tempList;
            unsigned choice = 0;
            for(unsigned i = 0; i < owner->getContainer()->getSize(); i++)
            {
                if(owner->getContainer()->getItem(i)->getPickable()->isID())
                    tempList.push_back(owner->getContainer()->getItem(i)->getName());
                else
                    tempList.push_back(owner->getContainer()->getItem(i)->getPickable()->getFakeName());
            }
            choice = graphicEngine.drawSelectionWindow("Examine which item?", tempList );
            if(choice != BADKEY)
            {
                displayDiscription(owner->getContainer()->getItem(choice));
            }else
            {
                if(owner->getContainer()->getSize() > 0)
                    messager.addMessage("Invalid choice", TCODColor::brass);
            }
        }
        if(key.c == 'g')
        {
            owner->getPos(tempPPos);
            for(unsigned i = 0; i < game->getList().size(); i++)
            {
                tempObject = game->getObject(i);
                tempObject->getPos(tempOPos);
                if((tempPPos[0] == tempOPos[0] && tempPPos[1] == tempOPos[1] ) && tempObject->getPickable())
                {
                    if(tempObject->getPickable()->pick(tempObject, owner))
                    {
                        if(tempObject->getPickable()->isID())
                            messager.addMessage("Picked up " + tempObject->getName()+".");
                        else
                            messager.addMessage("Picked up " + tempObject->getPickable()->getFakeName()+".");
                        owner->getAI()->changeEnergy(30);
                        pickedUp = true;
                        break;
                    }else
                    {
                        if(tempObject->getPickable()->isID())
                            messager.addMessage("Couldn't pick up " + tempObject->getName()+".");
                        else
                            messager.addMessage("Couldn't pick up " + tempObject->getPickable()->getFakeName()+".");
                    }

                }
            }
            if(pickedUp == false)
            {
                messager.addMessage("Nothing to pick up.", TCODColor::brass);
            }

        }
        if(key.c == 'a')
        {
            std::vector<std::string> tempList;
            unsigned choice = 0;
            for(unsigned i = 0; i < owner->getContainer()->getSize(); i++)
            {
                if(owner->getContainer()->getItem(i)->getPickable()->isID())
                    tempList.push_back(owner->getContainer()->getItem(i)->getName());
                else
                    tempList.push_back(owner->getContainer()->getItem(i)->getPickable()->getFakeName());
            }
            choice = graphicEngine.drawSelectionWindow("Use which item?", tempList );
            if(choice != BADKEY)
            {
                tempObject = owner->getContainer()->getItem(choice);
                if(tempObject->getPickable()->use(tempObject, owner))
                    owner->getAI()->changeEnergy(40);
                else if(tempObject)
                {
                    if(tempObject->getPickable()->hasEffect() == false)
                        messager.addMessage("You can't use that...", TCODColor::brass);
                    else
                        messager.addMessage("That had no effect...", TCODColor::brass);
                }
                tempObject = NULL;


            }else
            {
                if(owner->getContainer()->getSize() > 0)
                    messager.addMessage("Invalid choice", TCODColor::brass);
            }
        }
        if(key.c == 'd')
        {
            std::vector<std::string> tempList;
            unsigned choice = 0;
            for(unsigned i = 0; i < owner->getContainer()->getSize(); i++)
            {
                if(owner->getContainer()->getItem(i)->getPickable()->isID())
                    tempList.push_back(owner->getContainer()->getItem(i)->getName());
                else
                    tempList.push_back(owner->getContainer()->getItem(i)->getPickable()->getFakeName());
            }
            choice = graphicEngine.drawSelectionWindow("Drop which item?", tempList );
            if(choice != BADKEY)
            {
                tempObject = owner->getContainer()->getItem(choice);
                tempObject->getPickable()->drop(tempObject, owner);
                tempObject = NULL;
                owner->getAI()->changeEnergy(30);

            }else
            {
                if(owner->getContainer()->getSize() > 0)
                    messager.addMessage("Invalid choice", TCODColor::brass);
            }
        }
        if(key.c == 'w')
        {
            std::vector<object*> tempList;
            std::vector<std::string> tempNameList;
            unsigned choice = 0;
            for(unsigned i = 0; i < owner->getContainer()->getSize(); i++)
            {
                tempObject = owner->getContainer()->getItem(i);
                if(tempObject->getPickable()->isID() && tempObject->getPickable()->_wearable)
                {
                    tempList.push_back(tempObject);
                    tempNameList.push_back(tempObject->getName());
                }
                else if(tempObject->getPickable()->_wearable)
                {
                    tempList.push_back(tempObject);
                    tempNameList.push_back(tempObject->getPickable()->getFakeName());
                }
                tempObject = NULL;
            }
            choice = graphicEngine.drawSelectionWindow("Wear which item?", tempNameList );
            if(choice != BADKEY)
            {
                tempObject = tempList[choice];
                tempObject->getPickable()->_wearable->wear(tempObject, owner);
                tempObject = NULL;
                owner->getAI()->changeEnergy(40);

            }else
            {
                if(owner->getContainer()->getSize() > 0)
                    messager.addMessage("Invalid choice", TCODColor::brass);
            }
        }
        if(key.c == 'r')
        {
            messager.addMessage("Placeholder for removing equiped items.");std::vector<object*> tempList;
            std::vector<std::string> tempNameList;
            unsigned choice = 0;
            for(unsigned i = 0; i < owner->getEquip()->getSize(); i++)
            {
                tempObject = owner->getEquip()->getItem(i);
                if(tempObject != NULL && tempObject->getPickable()->isID())
                {
                    tempList.push_back(tempObject);
                    tempNameList.push_back(tempObject->getName());
                }
                else if(tempObject != NULL)
                {
                    tempList.push_back(tempObject);
                    tempNameList.push_back(tempObject->getPickable()->getFakeName());
                }
                tempObject = NULL;
            }
            choice = graphicEngine.drawSelectionWindow("Remove which item?", tempNameList );
            if(choice != BADKEY)
            {
                tempObject = tempList[choice];
                tempObject->getPickable()->_wearable->unwear(tempObject, owner);
                tempObject = NULL;
                owner->getAI()->changeEnergy(30);

            }else
            {
                if(owner->getContainer()->getSize() > 0)
                    messager.addMessage("Invalid choice", TCODColor::brass);
            }
        }
    }









        if(key.c == 'z')
        {
            messager.addMessage("Placeholder for using spells");
        }

        if(key.c == '@')
        {
            messager.addMessage("Placeholder for a status screen");
        }

        if(key.c == 'x')
        {
            messager.addMessage("Placeholder for examining");
        }

        if(key.c == 'c')
        {
            messager.addMessage("Placeholder for closing doors.");
        }

        if(key.c == '?')
        {
            graphicEngine.drawPopupWindow("Help",helpMessage,63,23);
        }

        if(key.c == 'S')
        {
            //temp message key for checking message code.
            game->gainXP(100);
        }

    if(gs == GAMEMOVED)
    {
        if(playerAI::moveOrAttack(owner, currentMap, cX, cY))
        {
            ;//subtract energy for walking
        }
    }
    return;
}

bool playerAI::moveOrAttack(object* owner, map* currentMap, int cX, int cY)
{
    int tempPPos[2];
    int tempEPos[2];
    object* tempObject = NULL;

    owner->getPos(tempPPos);

    tempPPos[0] += cX;
    tempPPos[1] += cY;

    //check to see if the owner is going to attack or move
    if( !(currentMap->getTile(tempPPos[0], tempPPos[1])->isWalkable) )
    {
        messager.addMessage("Ouch! You ran into a wall!", TCODColor::brass);//send a message that you bonk'd into something
        changeEnergy(-40);
        return false;
    }else
    {
        //Aight, we arn't running into a wall, check to see if we're running into something we can attack
        for(unsigned i = 0; i < game->getList().size(); i++)
        {
            tempObject = game->getObject(i);
            tempObject->getPos(tempEPos);
            if(tempEPos[0] == tempPPos[0] && tempEPos[1] == tempPPos[1])
            {   //this object is in the way!
                if( (tempObject->getDestroyable() != NULL) && (owner->getDamager() != NULL) )
                {   //This object is in the way and can be attacked, and we can attack.

                    //time to attack it!
                    owner->getDamager()->attack(owner, tempObject);
                    changeEnergy(-70);
                    return false;
                }
                //check to see if this object blocks pathing!
                if( tempObject->isBlocking() )
                {
                    //Oh snap, this object blocks pathing. You bumped yo!
                    messager.addMessage("Ouch! You ran into something!", TCODColor::brass);
                    changeEnergy(-40);
                    return false;
                }
            }
        }
        owner->movePos(cX, cY);
        changeEnergy(-50);
        return true;
    }
}

genericMonsterAI::genericMonsterAI(int sightRadius, int chaseTurns, int eCharge) :
    AI(eCharge), _sightRadius(sightRadius), _chaseTurns(chaseTurns), _maxChaseTurns(chaseTurns), _hasSeen(false)
{
    ;
}

void genericMonsterAI::update(object* owner, gameState& gs, map* currentMap)
{
    int tempPPos[2], tempOPos[2];
    if(owner->getDamager()) owner->getDamager()->calcAttacks(owner);
    bool inSight = false;
    if(owner->getTraits() && owner->getTraits()->_onUpkeep)
    {
        owner->getTraits()->_onUpkeep->apply->applyTo(owner, owner);
    }
    //check to see if we have the energy to take a turn
    if(_energy == 100)
    {
        game->getPlayer()->getPos(tempPPos);
        owner->getPos(tempOPos);
        float distance = calcLineDistance(tempPPos[0], tempPPos[1], tempOPos[0], tempOPos[1]);
        //generic monster ai. Will check for certain flags to modify its base behavior.
        if( (distance > float(_sightRadius) ) )//need to get FOV in here)
        {
            //out of sight. If we arn't chasing anymore, idle. If we've got chase turns left, keep chasing.
            if( !_hasSeen || _chaseTurns < 0)
            {
                return;
            }else
            {
                _chaseTurns -= 1;
            }
        }else
        {
            //ok! player in sight!
            inSight = true;
            _hasSeen = true;
            game->getPlayer()->getPos(_lastPPos);
            _chaseTurns = _maxChaseTurns;
        }
        if(inSight)
        {
            moveOrAttack(owner, currentMap, tempPPos[0], tempPPos[1]);//calc movment based on players actual position.

        }else if(_hasSeen)
        {
            if(tempOPos[0] != _lastPPos[0] && tempPPos[1] != _lastPPos[1])
                moveOrAttack(owner, currentMap, _lastPPos[0], _lastPPos[1]);//calc movement based on last place seen.
            else
                wander(owner, currentMap);
        }else
        {
            return;
        }
    }
}

void genericMonsterAI::moveOrAttack(object* owner, map* currentMap, int cX, int cY)
{
    int tempPPos[2];
    owner->getPos(tempPPos);
    //check to see if we need to move
    float distance = calcLineDistance(tempPPos[0], tempPPos[1] , cX, cY);
    int tempX, tempY;
    if(distance >= 2)
    {
        //we arn't close enough to attack. Get closer!
        tempX = int(round(cX - tempPPos[0])/(int)(distance));
        tempY = int(round(cY - tempPPos[1])/(int)(distance));

        //check to see if monster can walk to that tile.

        if( currentMap->isWalkable(tempPPos[0] + tempX, tempPPos[1] + tempY))
        {
            owner->movePos(tempX, tempY);
            changeEnergy(-50);
            return;
        }
        else
        {
            //can't walk to that tile. Time to slide around.
            if(tempX != 0)
            {
                if( currentMap->isWalkable(tempPPos[0] + tempX, tempPPos[1] - 1))
                {
                    owner->movePos(tempX, -1);//Go here instead!
                    changeEnergy(-50);
                    return;
                }else if( currentMap->isWalkable(tempPPos[0] + tempX, tempPPos[1]) )
                {
                    owner->movePos(tempX, 0);//Go here instead!
                    changeEnergy(-50);
                    return;
                }else if( currentMap->isWalkable(tempPPos[0] + tempX, tempPPos[1] + 1) )
                {
                    owner->movePos(tempX, 1);//Go here instead!
                    changeEnergy(-50);
                    return;
                }

            }else if(tempY != 0)
            {
                if( currentMap->isWalkable(tempPPos[0] - 1, tempPPos[1] + tempY) )
                {
                    owner->movePos(-1, tempY);//Go here instead!
                    changeEnergy(-50);
                    return;
                }else if( currentMap->isWalkable(tempPPos[0], tempPPos[1] + tempY) )
                {
                    owner->movePos(0, tempY);//Go here instead!
                    changeEnergy(-50);
                    return;
                }else if( currentMap->isWalkable(tempPPos[0] + 1, tempPPos[1] + tempY) )
                {
                    owner->movePos(+1, tempY);//Go here instead!
                    changeEnergy(-50);
                    return;
                }
            }
        }
        //couldn't walk anywhere. :/
        changeEnergy(-40);
    }else
    {
        //We close enough, we attacking!
        messager.addMessage(owner->getName() + " tries to attack!");
        changeEnergy(-75);
    }
}

void genericMonsterAI::wander(object* owner, map* currentMap)
{
    //we wandering!
    TCODRandom* rand = new TCODRandom;
    int tempPPos[2];
    int cX, cY;

    owner->getPos(tempPPos);

    cX = rand->getInt(-1, 1);
    cY = rand->getInt(-1, 1);

    if( currentMap->isWalkable(tempPPos[0] + cX, tempPPos[1] + cY) )
    {
        owner->movePos(cX, cY);//Go here instead!
        changeEnergy(-50);
        return;
    }else if( currentMap->isWalkable(tempPPos[0] - cX, tempPPos[1] + cY) )
    {
        owner->movePos(-cX, cY);//Go here instead!
        changeEnergy(-50);
        return;
    }else if( currentMap->isWalkable(tempPPos[0] + cX, tempPPos[1] - cY) )
    {
        owner->movePos(cX, -cY);//Go here instead!
        changeEnergy(-50);
        return;
    }else if( currentMap->isWalkable(tempPPos[0] - cX, tempPPos[1] - cY) )
    {
        owner->movePos(-cX, -cY);//Go here instead!
        changeEnergy(-50);
        return;
    }
    delete rand;

}

void playerAI::displayDiscription(object* item)
{
    unsigned sizeX, sizeY;
    sizeX = 30;
    sizeY = item->getDescription().size();
    sizeY = sizeY / sizeX;
    if(sizeY < 10)
    {
        //ten is the lowest Y size I'll take.
        sizeY = 10;
    }else
    {
        sizeY += 4;
    }
    graphicEngine.drawPopupWindow(item->getName(),item->getDescription(), sizeX, sizeY);

}
