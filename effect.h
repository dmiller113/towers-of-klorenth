#ifndef EFFECT_H
#define EFFECT_H

//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//effect.h

//Includes
#include "object.h"

//Defines


//Globals


//Enums


//Classes


class effect
{
public:
    effect();
    ~effect();
    virtual bool applyTo(object* target, object* source) = 0;
    virtual effect* getCopy( void ) = 0;
protected:
};

class changeHealthEffect : public effect
{
    int _amount;
    damageTypes _type;
    std::string _message;
public:
    changeHealthEffect(int amount, damageTypes type, std::string report);
    changeHealthEffect( changeHealthEffect* old );
    changeHealthEffect* getCopy( void );
    bool applyTo(object* target, object* source);
};

class changeLightRadiusEffect : public effect
{
    int _amount;
    std::string _message;
public:
    changeLightRadiusEffect(int amount, std::string report);
    bool applyTo(object* target, object* source);
    changeLightRadiusEffect* getCopy( void );
};
#endif // EFFECT_H
