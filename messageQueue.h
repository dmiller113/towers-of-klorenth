//Towers of Klorenth, 7dRL
//Writen designed and planned by Daniel Miller, 2013
//messageQueue.h

#ifndef MESSAGEQUEUE_H
#define MESSAGEQUEUE_H

//Includes
#include "libtcod.hpp"
#include "defines.h"
#include <string>
#include <vector>

//Defines


//Globals


//Enums


//Classes


//Functions

struct message
{
    TCODColor messageColor;
    std::string messageText;
};

class messageQueue
{
    std::vector<message*> _messageList;
public:
    messageQueue();
    ~messageQueue();
    void addMessage( std::string msgTxt, TCODColor msgColor = TCODColor::white);
    unsigned getSize( void ) {return _messageList.size();}
    message* getMessage( unsigned pos );
};

extern messageQueue messager;

#endif // MESSAGEQUEUE_H
