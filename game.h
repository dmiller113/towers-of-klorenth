//Towers of Klorenth, 7drl 2013 by Daniel Miller
//game.h
//Holds the funcitons related to the gameloop. H file yo.

#ifndef GAME_H
#define GAME_H

//includes
#include "libtcod.hpp"
#include "defines.h"

void initGame( bool isNew = true);
void gameLoop( void );
void cleanGame( void );


#endif // GAME_H
