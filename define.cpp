#include "defines.h"

float getDistance(float x1, float y1, float x2, float y2)
{
    float answer = 0;
    float dX = (x2 - x1);
    float dY = (y2 - y1);
    answer = dX * dX + dY * dY;
    answer = sqrtf(answer);
    return answer;
}
