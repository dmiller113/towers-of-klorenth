#ifndef TARGETSELECTOR_H
#define TARGETSELECTOR_H

//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//TargetSelector.h

//Includes
#include <vector>
#include "object.h"

//Defines


//Globals


//Enums
enum SelectorType
{
    CLOSEST_MONSTER,
    SELECTED_TILE,
    SELECTED_MONSTER,
    PBAOE
};

//Classes


class TargetSelector
{
public:
    TargetSelector(SelectorType type, float range);
    void selectTargets(object* source, std::vector<object*>& listOfObjects );
    ~TargetSelector();
    SelectorType getType(void) {return _type;}
    float getRange(void) {return _range;}
protected:
    SelectorType _type;
    float _range;
};

#endif // TARGETSELECTOR_H
