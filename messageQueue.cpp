//Towers of Klorenth, 7dRL
//Writen designed and planned by Daniel Miller, 2013
//messageQueue.cpp

//Includes
#include "messageQueue.h"

//Defines


//Globals


//Enums


//Classes


//Functions



messageQueue::messageQueue()
{
    //ctor
}

messageQueue::~messageQueue()
{
    //dtor
    for(unsigned i = 0; i < _messageList.size(); i++) //Time to clean up like a good person.
    {
        if(_messageList[i] != NULL)
            delete _messageList[i];
    }
}

void messageQueue::addMessage( std::string msgTxt, TCODColor msgColor )
{
    message* tempMessage;
    tempMessage = new message();
    tempMessage->messageColor = msgColor;
    tempMessage->messageText = msgTxt;

    if(msgTxt.length() < (MAXWINDOWX - 2) ) //Check to see if we'll fit on one line.
    {
        if(_messageList.size() < MESSAGEY - 3) //check to see that the message queue isn't full
        {
            //we arn't full, just pop the message in.
            _messageList.push_back(tempMessage);
            return;
        }else
        {
            //Uh oh, list is full. Need to get rid of the earliest message to make room.
            delete _messageList[0];
            for(unsigned i = 0; i < _messageList.size() - 1 ; i++)
            {
                _messageList[i] = _messageList[i + 1];
            }
            _messageList.pop_back();
            _messageList.push_back(tempMessage);
            return;
        }
    }else
    {
        //text is more than one line.
        tempMessage->messageText = msgTxt.substr(0, MAXWINDOWX - 3);
        addMessage(tempMessage->messageText, tempMessage->messageColor);
        addMessage(msgTxt.substr(MAXWINDOWX - 3), msgColor);    //should make it so we don't go out of bounds.
    }
}

message* messageQueue::getMessage( unsigned pos )
{
    return _messageList[pos];
}
