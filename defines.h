#ifndef DEFINE_H
#define DEFINE_H

//Towers of Klorenth, 7drl 2013 by Daniel Miller
//Defines.h
//Holds useful global defines/constnats/enums

#include <cmath>

//Constants
const unsigned MAXFPS = 24;
const unsigned MAXWINDOWX = 80;
const unsigned MAXWINDOWY = 60;
const unsigned MENUMAXX = MAXWINDOWX - 10;
const unsigned MENUMAXY = 30;
const unsigned MENUMINX = 10;
const unsigned MENUMINY = 2;
const unsigned MESSAGEY = 12;
const unsigned UIMAXX = 20;
const unsigned UIMAXY = MAXWINDOWY - MESSAGEY;
const unsigned MAPMAXX = MAXWINDOWX - UIMAXX;
const unsigned MAPMAXY = MAXWINDOWY - MESSAGEY;
const unsigned BADKEY = 9999;

//Enums

enum menuChoice
{
    MENUNEWGAME,
    MENULOADGAME,
    MENUOPTIONS,
    MENUEXIT,
    MENUERROR
};

enum gameState
{
    GAMEIDLE,
    GAMEOVER,
    GAMEERROR,
    GAMEMOVED,
    GAMEPLAYERACT,
    GAMESTAIR
};

enum damageTypes
{
    DMGPHYSICAL,
    DMGMENTAL,
    DMGSTATUS,
    DMGTRUE
};

enum equipLocations
{
    EQUIPWEAPON,
    EQUIPBODY,
    EQUIPRING,
    EQUIPNECK,
    EQUIPMISC,
    EQUIPNONE
};

extern gameState gs;

//functions

float getDistance(float x1, float y1, float x2, float y2);


#endif // DEFINE_H
