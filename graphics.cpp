//Towers of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//graphics.cpp

//Includes
#include "graphics.h"
#include "defines.h"
#include "gameEngine.h"
//Defines


//Globals


//Enums


//Classes


//Functions



graphics::graphics() :
    _mainScreen(NULL), _messages(NULL), _UI(NULL), _fov(NULL), _viewDistance(5), _fovDirty(true)
{
    //ctor
    _mainScreen = new TCODConsole(MAPMAXX, MAPMAXY);
    _messages = new TCODConsole(MAXWINDOWX - 2, MESSAGEY - 3);
    _UI = new TCODConsole(UIMAXX, UIMAXY);
    _fov = new TCODMap(MAPMAXX, MAPMAXY);

}

graphics::~graphics()
{
    //dtor
    if(_mainScreen != NULL)
    {
        delete _mainScreen;
    }
    if(_messages != NULL)
    {
        delete _messages;
    }
    if(_UI != NULL)
    {
        delete _UI;
    }
    if(_fov != NULL)
    {
        delete _fov;
    }
}

void graphics::render(object& player, map& currentMap, std::vector<object*> objList)
{
    //clear the root screen
    TCODConsole::root->clear();
    drawLines();//Draw the Lines seperating the parts
    drawUI(player);//Draw the UI
    drawMap(currentMap);//Draw the map
    drawObjects(objList);//Draw the objects
    drawPlayer(player);//Draw the player
    drawMessages(); //Draw the messages

    //blits, blits for everyone!
    TCODConsole::blit(_mainScreen, 0,0,0,0, TCODConsole::root, 1, 1);
    TCODConsole::blit(_UI, 0,0,0,0, TCODConsole::root, MAPMAXX + 2, 1);;//blit the UI
    TCODConsole::blit(_messages, 0,0,0,0, TCODConsole::root, 1, MAPMAXY + 2);;//blit the messages

    //flush that stuff!
    TCODConsole::flush();

}

void graphics::drawMap(map& currentMap)
{
    _mainScreen->clear();
    for(unsigned x = 0; x < MAPMAXX; x++)
    {
        for(unsigned y = 0; y < MAPMAXY; y++)
        {
            if(_fov->isInFov(x, y))
            {
                _mainScreen->putCharEx(x, y, currentMap.getTile(x,y)->_symbol,
                    currentMap.getTile(x,y)->_foreColor, currentMap.getTile(x,y)->_backColor);
                currentMap.getTile(x, y)->isExplored = true;
            }else if(currentMap.getTile(x, y)->isExplored == true)
            {
                _mainScreen->putCharEx(x, y, currentMap.getTile(x,y)->_symbol,
                    TCODColor::darkerGrey, TCODColor::black);
            }
        }
    }
}

void graphics::drawPlayer(object& player)
{
    player.draw(_mainScreen);
}

void graphics::drawLines( void )
{
    for(unsigned x = 0; x < MAXWINDOWX; x++)
    {
        for(unsigned y = 0; y < MAXWINDOWY; y++)
        {
            if(x == 0 || x == MAXWINDOWX - 1 || (x == MAPMAXX + 1 && y <= MAPMAXY))
            {
                TCODConsole::root->putCharEx(x, y, '|', TCODColor::white, TCODColor::black);
            }
            if(y == 0 || y == MAXWINDOWY - 1 || y == MAPMAXY + 1)
            {
                TCODConsole::root->putCharEx(x, y, '-', TCODColor::white, TCODColor::black);
            }

        }
    }
}

void graphics::remakeFOV(map& currentMap)
{
    for(unsigned x = 0; x < MAXWINDOWX; x++)
    {
        for(unsigned y = 0; y < MAXWINDOWY; y++)
        {
            _fov->setProperties(x, y, currentMap.getTile(x,y)->isTransparent, currentMap.getTile(x,y)->isWalkable);
        }
    }
}

void graphics::computeFOV( object* player )
{
    int temp[2];
    player->getPos(temp);
    _fov->computeFov(temp[0],temp[1], _viewDistance, true);
    _fovDirty = false;
}

void graphics::drawObjects(std::vector<object*> objList)
{
    static int temp[2];
    for(unsigned i = 0; i < objList.size(); i++)//draw dem lists.
    {
        objList[i]->getPos(temp);
        if( _fov->isInFov(temp[0], temp[1]) )
        {
            objList[i]->draw(_mainScreen);
        }
    }
}

void graphics::drawMessages( void )
{
    _messages->clear();
    message* tempMessage;
    TCODColor oldFore = _messages->getDefaultForeground();
    for(unsigned i = 0; i < messager.getSize(); i++)
    {
        tempMessage = messager.getMessage(i);
        _messages->setDefaultForeground(tempMessage->messageColor);
        _messages->print(0, i, tempMessage->messageText.c_str() );
    }
    _messages->setDefaultForeground(oldFore);
}

void graphics::drawUI( object& player )
{
    TCODColor oldFore = _UI->getDefaultForeground();
    _UI->clear();
    //Time to draw the UI for the player. Draws their name, stats and equipment.

    //Name!
    _UI->printEx(7, 1, TCOD_BKGND_NONE, TCOD_CENTER, player.getName().c_str());

    //Display experience and level
    _UI->print(1, 2, "Level: %d", game->getPLevel());
    _UI->print(1, 3, "%d/%d EXP", game->getPEXP(), game->returnNextLevel());

    //Dungeon level
    _UI->print(0, 4, "Exploring level %d", game->getLevel());

    //Time to put down the stats of the player

    //first up is their vitals: Health Stamina and Belief
    _UI->setDefaultForeground(TCODColor::crimson);
    _UI->print(1, 6, "HP: %d/%d", player.getDestroyable()->getCurrentHP(), player.getDestroyable()->getMaxHP());//health

    _UI->setDefaultForeground(TCODColor::amber);
    _UI->print(1, 7, "Stamina: %d/100", player.getAI()->getStamina() );//stamina

    _UI->setDefaultForeground(TCODColor::sky);
    _UI->print(1, 8, "Belief:  %d", player.getDamager()->getBelief() );//belief

    //then is their base stats: Strength, Dexterity, and Willpower
    _UI->setDefaultForeground(TCODColor::white);
    _UI->print(1, 11, "Strength:  %d", player.getDamager()->getStr() );//strength
    _UI->print(1, 12, "Dexterity: %d", player.getDamager()->getDex() );;//dexterity
    _UI->print(1, 13, "Willpower: %d", player.getDamager()->getWill() );;//willpower

    //Then their derived stats: Physical and Mental Attack, Physical and Mental Defense
    _UI->setDefaultForeground(TCODColor::orange);
    _UI->print(1, 16, "Physical Atk: %d", player.getDamager()->getPhysicalAttack() );//Physical Attack
    _UI->print(1, 17, "Mental Atk:   %d", player.getDamager()->getMentalAttack() );//Mental Attack
    _UI->setDefaultForeground(TCODColor::lime);
    _UI->print(1, 19, "Physical Def: %d", player.getDestroyable()->getPhysDef() );//Physical Defense
    _UI->print(1, 20, "Mental Def:   %d", player.getDestroyable()->getMentalDef() );//Mental Attack
    _UI->setDefaultForeground(TCODColor::white);

    //Then their Equipement: Need to program in equipment :|
    std::string tempString;
    if(player.getEquip()->getItem(EQUIPWEAPON))
        tempString = "W: " + player.getEquip()->getItem(EQUIPWEAPON)->getName();
    else
        tempString = "W: Unequipped";
    _UI->print(2, 24, tempString.c_str());//Weapon

    if(player.getEquip()->getItem(EQUIPBODY))
        tempString = "B: " + player.getEquip()->getItem(EQUIPBODY)->getName();
    else
        tempString = "B: Unequipped";
    _UI->print(2, 25, tempString.c_str());//Armor

    if(player.getEquip()->getItem(EQUIPRING))
        tempString = "A1: " + player.getEquip()->getItem(EQUIPRING)->getName();
    else
        tempString = "A1: Unequipped";
    _UI->print(1, 26, tempString.c_str() );//Accessory 1 (Rings)

    if(player.getEquip()->getItem(EQUIPNECK))
        tempString = "A2: " + player.getEquip()->getItem(EQUIPNECK)->getName();
    else
        tempString = "A2: Unequipped";
    _UI->print(1, 27, tempString.c_str() );//Accessory 2 (Neck)

    if(player.getEquip()->getItem(EQUIPMISC))
        tempString = "A3: " + player.getEquip()->getItem(EQUIPMISC)->getName();
    else
        tempString = "A3: Unequipped";
    _UI->print(1, 28, tempString.c_str() );//Accessory 3 (Free)

    //should be done.
    _UI->setDefaultForeground(oldFore);
}

void graphics::drawPopupWindow(const std::string& title, const std::string& message, unsigned sizeX, unsigned sizeY, bool waitForKey)
{
    TCODConsole::flush();
    TCODConsole* popup = new TCODConsole(sizeX, sizeY);
    popup->setDefaultBackground(TCODColor::blue);
    popup->clear();

    for(unsigned x = 0; x < sizeX; x++)
    {
        for(unsigned y = 0; y < sizeY; y++)
        {
            if(x == 0 && y == 0)
            {
                popup->putChar(x, y, TCOD_CHAR_DNW, TCOD_BKGND_NONE);
            }else if(x == (sizeX - 1) && y == 0)
            {
                popup->putChar(x, y, TCOD_CHAR_DNE, TCOD_BKGND_NONE);
            }else if(x == 0 && y == (sizeY - 1))
            {
                popup->putChar(x, y, TCOD_CHAR_DSW, TCOD_BKGND_NONE);
            }else if(x == (sizeX - 1) && y == (sizeY - 1))
            {
                popup->putChar(x, y, TCOD_CHAR_DSE, TCOD_BKGND_NONE);
            }else if(y == 0 || y == (sizeY - 1) )
            {
                popup->putChar(x, y, TCOD_CHAR_DHLINE, TCOD_BKGND_NONE);
            }else if(x == 0 || x == (sizeX - 1) )
            {
                popup->putChar(x, y, TCOD_CHAR_DVLINE, TCOD_BKGND_NONE);
            }
        }
    }
    //print out the title and message
    if(title.size() < (sizeX - 2) )
    {
        popup->print( ( sizeX - title.size() ) / 2, 0, title.c_str() );

    }

    //Title done, time for the actual message
    popup->printRect(1,1, sizeX - 2, sizeY - 2, message.c_str());

    TCODConsole::blit(popup,0,0,0,0, TCODConsole::root, ((MAXWINDOWX - sizeX)/2), 5, 1, .9 );
    TCODConsole::flush();
    if(waitForKey)
        TCODConsole::waitForKeypress(true);
    return;
}

unsigned graphics::drawSelectionWindow(const std::string& title, std::vector<std::string> choices)
{
    //Figure out how big the window should be
    unsigned sizeX, sizeY;
    sizeX = 30;
    sizeY = choices.size() + 3 > 10?choices.size() + 3:10;
    std::string tempString("");
    char letter = 'a';
    unsigned result = 0;
    if(choices.size() > 0)
    {
        for(unsigned i = 0; i < choices.size(); i++)
        {
            if(sizeX < choices[i].size() )
            {
                sizeX = choices[i].size();
            }
            tempString = tempString + letter + ") " + choices[i] + "\n";
            letter += 1;
        }
    }else
    {
        tempString = " Empty";
        sizeX = 30;

    }
    //aite, made the string the sizes, now we send for the window.
    drawPopupWindow(title, tempString, sizeX, sizeY, false);

    //aight get the value of the item choice
    TCOD_key_t key = TCODConsole::waitForKeypress(true);
    if(key.c != 0)
    {
        result = key.c -'a';
        if(result >= choices.size())
            result = BADKEY;
    }else
    {
        result = BADKEY;
    }
    return result;

}
