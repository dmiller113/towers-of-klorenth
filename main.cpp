//Towers of Klorenth, Daniel Miller 2013 7drl
//Main.cpp

#include "libtcod.hpp"
#include "defines.h"
#include "game.h"
#include <iostream>

bool initProgram(void);
menuChoice menuHandle(void);
void drawCursor(menuChoice);
bool cleanProgram(void);
void drawMenu(void);
menuChoice handleMenuKey(const TCOD_key_t& key, const menuChoice& oldChoice);


int main(void)
{
    std::cout<<"I need an instruction";
    //initialize program
    if(!initProgram())
    {
        //wish I had actual error checking techniques, as something's wrong here
        return -1;
    }
    //handle menu
    menuChoice choice = menuHandle();

    //check menu choice
    switch (choice)
    {
        case MENULOADGAME:
        {
            //initGame(false);
            break;//Load game!
        }
        case MENUNEWGAME:
        {
            initGame(true);
            break;//Start a new game!
        }
        case MENUEXIT:
        {
            if( !cleanProgram() )
            {
                ;//more error code. But I don't have any.
            }
            return 0;
            break;//Oh snaps, they don't want to play
        }
        case MENUOPTIONS:
        {
            break;//Not sure how I'm going to handle this!
        }
        case MENUERROR:
        {
            break;//Real programers would have robust error code here!
        }
        default:
        {
            break;//Urm, no idea how I ended here.
        }
    }


    //if game, initialize game

    //play game
    gameLoop();

    //game over, cleanup game
    cleanProgram();
    //clean up program
    if( !cleanProgram() )
    {
        ;//more error code. But I don't have any.
    }
    //Aight, time to end it!
    return 0;
}

bool initProgram(void)
{
    TCODConsole::initRoot(MAXWINDOWX, MAXWINDOWY, "Towers of Klorenth");
    TCODSystem::setFps(MAXFPS);

    return true;
}

menuChoice menuHandle(void)
{
    static menuChoice choice = MENUNEWGAME;
    static TCOD_key_t key;
    do
    {
        TCODConsole::root->clear();
        drawMenu(); //Draw the menu,
        drawCursor(choice);
        TCODConsole::flush();
        //handle the input
        key = TCODConsole::checkForKeypress(TCOD_KEY_PRESSED);
        choice = handleMenuKey(key, choice);
    }while( (key.vk != TCODK_ENTER && key.vk != TCODK_KPENTER) && !TCODConsole::isWindowClosed() );

    return choice;

}

menuChoice handleMenuKey(const TCOD_key_t& key, const menuChoice& oldChoice)
{
    switch (oldChoice)
    {
        case MENULOADGAME:
        {
            if(key.vk == TCODK_LEFT || key.vk == TCODK_KP4)
            {
                return MENUNEWGAME;
            }else if(key.vk == TCODK_RIGHT || key.vk == TCODK_KP6)
            {
                return MENUOPTIONS;
            }
            break;
        }
        case MENUOPTIONS:
        {
            if(key.vk == TCODK_LEFT || key.vk == TCODK_KP4)
            {
                return MENULOADGAME;
            }else if(key.vk == TCODK_RIGHT || key.vk == TCODK_KP6)
            {
                return MENUEXIT;
            }
            break;
        }
        case MENUNEWGAME:
        {
            if(key.vk == TCODK_LEFT || key.vk == TCODK_KP4)
            {
                return oldChoice;
            }else if(key.vk == TCODK_RIGHT || key.vk == TCODK_KP6)
            {
                return MENULOADGAME;
            }
            break;
        }
        case MENUEXIT:
        {
            if(key.vk == TCODK_LEFT || key.vk == TCODK_KP4)
            {
                return MENUOPTIONS;
            }else if(key.vk == TCODK_RIGHT || key.vk == TCODK_KP6)
            {
                return oldChoice;
            }
            break;
        }
        default:
            return oldChoice;
            break;
    }
    return oldChoice;
}

void drawMenu(void)
{
    for(unsigned y = 0; y < MAXWINDOWY; y++)
    {
        for(unsigned x = 0; x < MAXWINDOWX; x++)
        {
            if( (y == MENUMAXY || y == MENUMINY ) && (x >= MENUMINX && x <= MENUMAXX) )
                TCODConsole::root->putCharEx(x, y, '-', TCODColor::white, TCODColor::black);

            if( (x == MENUMAXX || x == MENUMINX ) && (y >= MENUMINY && y <= MENUMAXY) )
                TCODConsole::root->putCharEx(x, y, '|', TCODColor::white, TCODColor::black);
        }
    }

    //Draw words
    TCODConsole::root->print(MENUMINX + 10, MENUMAXY + 10, "New Game");
    TCODConsole::root->print(MENUMINX + 21, MENUMAXY + 10, "Load Game");
    TCODConsole::root->print(MENUMINX + 33, MENUMAXY + 10, "Options");
    TCODConsole::root->print(MENUMINX + 44, MENUMAXY + 10, "Exit");

    //Draw Title
    //Wish I was good at ascii art, gonna have to stick with text
    TCODConsole::root->print(MENUMINX + 22, MENUMINY + 10, "Towers of");
    TCODConsole::root->print(MENUMINX + 28, MENUMINY + 11, "Klorenth");
    TCODConsole::root->print(MENUMINX + 27, MENUMINY + 13, "by");
    TCODConsole::root->print(MENUMINX + 24, MENUMINY + 15, "shinychaos");

}

void drawCursor(menuChoice choice)
{
    switch (choice)
    {
        case MENUNEWGAME:
        {
            TCODConsole::root->putChar(MENUMINX + 9, MENUMAXY + 10, TCOD_CHAR_ARROW2_E, TCOD_BKGND_NONE);
            break;
        }
        case MENULOADGAME:
        {
            TCODConsole::root->putChar(MENUMINX + 20, MENUMAXY + 10, TCOD_CHAR_ARROW2_E, TCOD_BKGND_NONE);
            break;
        }
        case MENUOPTIONS:
        {
            TCODConsole::root->putChar(MENUMINX + 32, MENUMAXY + 10, TCOD_CHAR_ARROW2_E, TCOD_BKGND_NONE);
            break;
        }
        case MENUEXIT:
        {
            TCODConsole::root->putChar(MENUMINX + 43, MENUMAXY + 10, TCOD_CHAR_ARROW2_E, TCOD_BKGND_NONE);
            break;
        }
    }
}

bool cleanProgram()
{
    return true;
}
