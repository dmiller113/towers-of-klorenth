//Towers of Klorenth, 7drl 2013 by Daniel Miller
//game.cpp
//Holds the funcitons related to the gameloop.

#include "game.h"
#include "gameEngine.h"
#include "graphics.h"
#include "messageQueue.h"
#include "effect.h"

gameEngine* game;
graphics graphicEngine;
messageQueue messager;
gameState gs = GAMEIDLE;

void initGame(bool isNew)
{
    //check to see if this is a new game or a loaded game we're setting up
    if(isNew)
    {
        //new game. Set up a new player character and send them to the new character stuff.
        //need to make a new map, with new items and enemies

        ;//Show intro plot

        ;//Create new character. Small choices, mainly asks what you want to focus on and gives stats and equip for that.

        ;//we're new, make a new seed for this map.
        game = new gameEngine();

    }else
    {
        //we got a load here. Need to get the seed for map creation, and get the all the data saved in the file and populate
        //it to the map/monsters/items/objects/yadayada

        ;//there's a proccess here, the logic is escaping me.

    }

    //initialize the game engine

    game->createMap();
    game->getMap()->populateMap();

    graphicEngine.remakeFOV( *(game->getMap() ) );
    graphicEngine.computeFOV(game->getPlayer());
    TCODConsole::root->clear();
    TCODConsole::flush();
}


void gameLoop( void )
{
    while( !TCODConsole::isWindowClosed() && gs != GAMEOVER )
    {
        TCODConsole::root->clear();
        game->checkLevel();
        graphicEngine.render( *(game->getPlayer()), *(game->getMap()), game->getList() );//draw screen
        game->tick(gs);//update the actors/objects
        if(gs == GAMEMOVED || gs == GAMESTAIR)
        {
            if(gs == GAMESTAIR)
                graphicEngine.remakeFOV(*game->getMap());
            graphicEngine.computeFOV(game->getPlayer());
            gs = GAMEIDLE;
        }
    }
}

void cleanGame( void )
{
    if(game)
        delete game;
}
