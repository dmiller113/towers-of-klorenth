//(Project), (Reason)
//Writen designed and planned by Daniel Miller, (Year)
//(filename)

//Includes


//Defines


//Globals


//Enums


//Classes


//Functions

#include "wearable.h"

wearable::wearable(equipLocations location, int physAtk, int physDef, int menAtk, int menDef, int dodgec,
             ability* hit, ability* attack, ability* bAttack, ability* bHit, ability* equip, ability* onRemove) :
    _location(location), _patk(physAtk), _pdef(physDef), _matk(menAtk), _mdef(menDef), _dodge(dodgec),
    _onHit(hit), _onAttack(attack), _onBAttack(bAttack), _onBHit(bHit), _onEquip(equip), _onRemove(onRemove)
{
    //ctor

}

wearable::~wearable()
{
    //dtor
    if(_onHit)
        delete _onHit;
    if(_onAttack)
        delete _onAttack;
    if(_onBHit)
        delete _onBHit;
    if(_onBAttack)
        delete _onBAttack;
    if(_onEquip)
        delete _onEquip;
    if(_onRemove)
        delete _onRemove;
}

bool wearable::wear(object* owner, object* equiper)
{
    bool success = false;
    int tempInt = int(_location);
    //check to see that the caller has a container with room
    if(equiper->getEquip())
    {
        if(owner == equiper->getEquip()->getItemList()[tempInt])
            return false;
        equiper->getEquip()->getItemList()[tempInt] = owner;
        if(_onEquip)
        {
            std::vector<object*> effectedObjects;

            if(_onEquip->selector)
            {
                _onEquip->selector->selectTargets(equiper, effectedObjects);
            }else
                effectedObjects.push_back(equiper);
            for(unsigned i = 0; i < effectedObjects.size(); i++)
            {
                if( _onEquip->apply->applyTo(effectedObjects[i], owner) )
                {
                    success = true;
                }
            }

        }
        return true;
    }
    return false;
}

bool wearable::unwear(object* owner, object* equiper)
{
    bool success = false;

    if(equiper->getEquip())
    {
        equiper->getEquip()->getItemList()[_location] = NULL;

        if(_onRemove)
        {
            std::vector<object*> effectedObjects;

            if(_onRemove->selector)
            {
                _onRemove->selector->selectTargets(equiper, effectedObjects);
            }else
                effectedObjects.push_back(equiper);
            for(unsigned i = 0; i < effectedObjects.size(); i++)
            {
                if( _onRemove->apply->applyTo(effectedObjects[i], owner) )
                {
                    success = true;
                }
            }

        }
        return true;
    }
    return false;
}

ability::~ability()
{
    if(selector)
        delete selector;
    if(apply)
        delete apply;
}

ability::ability(ability* old)
{
    if(old != NULL)
    {
        if(old->selector)
            selector = new TargetSelector(old->selector->getType(), old->selector->getRange());
        else
            selector = NULL;
        type = old->type;
        range = old->range;
        apply = old->apply->getCopy();
    }else
    {
        selector = NULL;
        range = 0;
        type = "";
        apply = NULL;
    }
}

ability::ability() :
    selector(NULL), range(0), apply(NULL), type("")
{
    ;
}
