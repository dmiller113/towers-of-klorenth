//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//specialTraits.cpp

//Includes
#include "specialTraits.h"

//Defines


//Globals


//Enums


//Classes


//Functions



specialTraits::specialTraits(std::vector<spell> spellBook, ability* onHit, ability* onAttack,
        ability* onBAttack, ability* onBHit, ability* onUpkeep) :
    _onHit(onHit), _onAttack(onAttack), _onBAttack(onBAttack), _onBHit(onBHit), _onUpkeep(onUpkeep)
{
    //ctor
}

specialTraits::~specialTraits()
{
    //dtor
    if( _onHit )
        delete _onHit;
    if( _onAttack )
        delete _onAttack;
    if( _onBAttack )
        delete _onBAttack;
    if( _onBHit )
        delete _onBHit;
    if( _onUpkeep )
        delete _onUpkeep;

    for(unsigned i = 0; i < spellBook.size(); i++)
    {
        if(spellBook[i])
            delete spellBook[i];
    }
}
