//Tower of Klorenth, 7dRL
//Writen designed and planned by Daniel Miller, 2013
//damager.h

//Includes

#include "damager.h"
#include "messageQueue.h"

//Defines


//Globals


//Enums


//Classes


//Functions


damager::damager(int str, int dex, int will, unsigned belief):
    _strength(str), _dexterity(dex), _willpower(will), _belief(belief)
{
    //ctor
}

damager::~damager()
{
    //dtor
}

void damager::calcAttacks( object* owner )
{
    _physicalAttack = _strength;
    if(owner->getEquip())
    {
        for(unsigned i = 0; i < owner->getEquip()->getSize(); i++)
        {
            if(owner->getEquip()->getItem(i))
            {
                _physicalAttack += owner->getEquip()->getItem(i)->getPickable()->_wearable->_patk;
            }
        }
    }
    _mentalAttack = _willpower; //temp formula. When Equipment is in, it will change significantly.
    if(owner->getEquip())
    {
        for(unsigned i = 0; i < owner->getEquip()->getSize(); i++)
        {
            if(owner->getEquip()->getItem(i))
            {
                _physicalAttack += owner->getEquip()->getItem(i)->getPickable()->_wearable->_matk;
            }
        }
    }
    return;
}

void damager::attack(object* owner, object* target)
{
    //WUT, ATTACKING TIME
    int grossDamage = 0;
    int diceRoll = 0;
    object* tempObject;
    TCODRandom* random = new TCODRandom;

    ;//Need to check for any abilities that trigger on attacking

    if(owner->getEquip())
    {
        tempObject = NULL;
        for(unsigned i = 0; i < owner->getEquip()->getSize(); i++)
        {
            tempObject = owner->getEquip()->getItem(i);
            if(tempObject)
            {
                if(tempObject->getPickable()->_wearable->_onAttack)
                {
                    tempObject->getPickable()->_wearable->_onAttack->apply->applyTo(target, owner);
                }
            }
        }
    }
    if(owner->getTraits())
    {
        if(owner->getTraits()->_onAttack)
        {
            owner->getTraits()->_onAttack->apply->applyTo(target, owner);
        }
    }

    grossDamage = _physicalAttack;

    //check to see if the attack hits
    diceRoll = random->getInt(1, 100);
    int dodgeChance = (target->getDestroyable()->getDodge() - _dexterity) > 0? (target->getDestroyable()->getDodge() - _dexterity): 0;
    if(diceRoll < (100 - dodgeChance * 5) )
    {
        ;//omg we hit. Check for abilities that trigger on hitting something
        //aight, actually damage the thing.
        if(owner->getEquip())
        {
            tempObject = NULL;
            for(unsigned i = 0; i < owner->getEquip()->getSize(); i++)
            {
                tempObject = owner->getEquip()->getItem(i);
                if(tempObject)
                {
                    if(tempObject->getPickable()->_wearable->_onHit)
                    {
                        if(tempObject->getPickable()->_wearable->_onHit->selector)
                            tempObject->getPickable()->_wearable->_onHit->apply->applyTo(target, owner);
                        else
                            tempObject->getPickable()->_wearable->_onHit->apply->applyTo(owner, target);
                    }
                }
            }
        }
        if(owner->getTraits())
        {
            if(owner->getTraits()->_onHit)
            {
                if(tempObject->getTraits()->_onHit->selector)
                    owner->getTraits()->_onHit->apply->applyTo(target, owner);
                else
                    owner->getTraits()->_onHit->apply->applyTo(owner, owner);
            }
        }
        target->getDestroyable()->takeDamage(target, owner, DMGPHYSICAL, grossDamage);
    }else
    {
        //Fawk, we missed. Check for abilities that trigger on misses
        ;
        //message that a miss occured.
        std::string temp = owner->getName() + " misses " + target->getName() +"!";
        messager.addMessage(temp, TCODColor::yellow);
    }


}
