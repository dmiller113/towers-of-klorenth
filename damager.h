//Tower of Klorenth, 7dRL
//Writen designed and planned by Daniel Miller, 2013
//damager.h

#ifndef DAMAGER_H
#define DAMAGER_H

//Includes
#include "object.h"
#include "libtcod.hpp"

//Defines


//Globals


//Enums


//Classes

class damager
{
protected:
    int _strength;
    int _dexterity;
    int _willpower;
    int _physicalAttack;
    int _mentalAttack;
    unsigned _belief;
public:
    damager(int str, int dex, int will, unsigned belief = 0);
    int getStr(void) {return _strength;}
    int getDex(void) {return _dexterity;}
    int getWill(void) {return _willpower;}
    int getPhysicalAttack(void) {return _physicalAttack;}
    int getMentalAttack(void) {return _mentalAttack;}
    unsigned getBelief(void) {return _belief;}
    void changeStr(int cValue) {_strength += cValue;}
    void changeDex(int cValue) {_dexterity += cValue;}
    void changeWill(int cValue) {_willpower += cValue;}
    void changeBelief(int cValue) {_belief += cValue;}
    void calcAttacks( object* owner );
    void attack(object* owner, object* target);
    ~damager();
};

#endif // DAMAGER_H
