!:Weak Health Potion:a bottle containing a strange liquid.:potion:red:black:false:false:0:0:0:0:0:0:1:consume|heal(self)(10): looks better! They heal for |#
!:Health Potion:a bottle containing a strange liquid.:potion:red:black:false:false:0:0:0:0:0:0:1:consume|heal(self)(25): looks better! They heal for |#
!:Strong Health Potion:a bottle containing a strange liquid.:potion:red:black:false:false:0:0:0:0:0:0:1:consume|heal(self)(50): looks better! They heal for |#
!:Poisoned Potion:a bottle containing a strange liquid.:potion:red:black:false:false:0:0:0:0:0:0:1:consume|pdamage(self)(10): looks sick! They are damaged for |#
!:Strong Poison Potion:a bottle containing a strange liquid.:potion:red:black:false:false:0:0:0:0:0:0:1:consume|pdamage(self)(20): looks sick! They are damaged for |#
!:Toxic Potion:a bottle containing a strange liquid.:potion:red:black:false:false:0:0:0:0:0:0:1:consume|pdamage(self)(40): looks sick! They are damaged for |#
?:Lightning Scroll:a dusty paper. Its covered in confusing writing and scribbles.:scroll:yellow:black:false:false:0:0:0:0:0:0:1:consume|pdamage(closestMonster)(7)(20): is struck by bolt of lightning! They are damaged for |#
?:Gale Scroll:a dusty paper. Its covered in confusing writing and scribbles.:scroll:azure:black:false:false:0:0:0:0:0:0:1:consume|pdamage(pbaoe)(4)(20): is struck by hail of ice! They are damaged for |#
?:Firebolt Scroll:a dusty paper. Its covered in confusing writing and scribbles.:scroll:red:black:false:false:0:0:0:0:0:0:1:consume|pdamage(targetMonster)(0)(20): is struck by stream of flame! They are damaged for |#
":Brass Lanturn:A dull brass lanturn. It seems like you could light it.:brass lanturn:amber:black:true:false:0:0:0:0:0:7:0:misc|none|none|none|none|none|light(self)(7): is brightly lit!|light(self)(5): dims.|#
[:Iron Sword: a shiny iron sword. Single bladed, and 3 foot long. It's blade looks sharp.:Iron Sword:grey:black:true:false:0:10:0:0:0:0:0:weapon|none|#
[:Blood Sword: a shiny iron sword. Single bladed, and 3 foot long. It's blade looks red in the light.:Iron Sword:crimson:black:true:false:0:10:0:0:0:0:0:weapon|none|none|heal(self)(3): looks refreshed. They heal for |#
[:Steel Sword: a shiny steel sword. Single bladed, and 3 foot long. It's blade looks sharp.:Steel Sword:lightgrey:black:true:false:0:30:0:0:0:0:0:weapon|none|#
[:Vampiric Sword: a shiny steel sword. Single bladed, and 3 foot long. It's blade looks red in the light.:Steel Sword:crimson:black:true:false:0:30:0:0:0:0:0:weapon|none|none|heal(self)(8): looks refreshed. They heal for |#
]:Leather Armor: A snug chestpiece of leather armor.:Leather Armor:brown:black:true:false:0:0:0:10:0:0:0:body|none|#
]:Plate Armor: A large chestpiece of plate armor.:Plate Armor:grey:black:true:false:0:0:20:0:0:0:0:body|none|#
]:Barbed Armor: A large chestpiece of spiked, plate armor.:Plate Armor:grey:black:true:false:0:0:20:0:0:0:0:body|none|#|none|none|none|pdamage(pbaoe)(1)(10): is knicked by the barbs! They take |#
[:Shock Sword: a shiny steel sword. Single bladed, and 3 foot long. It's blade crackles in the light.:Iron Sword:azure:black:true:false:0:25:0:0:0:0:0:weapon|none|none|mdamage(pbaoe)(15): is struck by lightning! They take |#
!:Explosion Potion:a bottle containing a strange liquid.:amber:potion:black:false:false:0:0:0:0:0:0:1:consume|pdamage(targetTile)(0)(30): is ravaged by fire! They are burned for |#
?:Blizzard Scroll:a dusty paper. Its covered in confusing writing and scribbles.:scroll:sky:black:false:false:0:0:0:0:0:0:1:consume|tdamage(targetTile)(0)(35): is struck by a blizzard! They are damaged for |#
&
//Order looks like this
//symbol:RealName:Description:FakeName:ForeColor:BackColor:IsEquip:IsPerm:Dodge:PAtk:Matk:PDef:MDef:SightRadius:Charges:Location|OnUseAbility(Target)(Amount):Message|OnAttack|OnHit|OnBAttack|OnBHit|OnEquip|OnRemove|#