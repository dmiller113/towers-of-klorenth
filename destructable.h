//Towers of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//destructable.h

#ifndef DESTRUCTABLE_H
#define DESTRUCTABLE_H

//Includes
#include "libtcod.hpp"
#include "object.h"
#include "defines.h"
#include <string>

//Defines


//Globals


//Enums


//Classes
class object;

class destructable
{
protected:
    unsigned _maxHp;
    int _currentHP;
    int _physicalDef;
    int _mentalDef;
    int _dodge;
    int _deadSymbol;
    int _worthXP;
    std::string _deadName;
public:
    destructable(unsigned maxHp, int physDef, int menDef, int xp, int dodge = 0, int deadChar = '%', std::string deadName = "");
    virtual void die(object* owner);
    int takeDamage(object* owner, object* source, damageTypes dmgType, int grossDamage, bool useMessage = true);
    int getCurrentHP(void) {return _currentHP;}
    int getMaxHP(void) {return _maxHp;}
    int getPhysDef(void) {return _physicalDef;}
    int getMentalDef(void) {return _mentalDef;}
    int getDodge(void) {return _dodge;}
    bool isDead(void) {return (_currentHP < 0);}
    int heal(object* owner, unsigned amount);
    int getXP(void) {return _worthXP;}
    void changeMaxHP(int cValue) {_maxHp += cValue;}
    int changeHP(int cValue, object* owner);
    int calcDamage(int amount, damageTypes type);
    virtual ~destructable();

};

class playerDestructable : public destructable
{
public:
    playerDestructable(unsigned maxHp, int physDef, int menDef, int xp, int dodge = 0, int deadChar = '%', std::string deadName = "");
    void die(object* owner);
};

class genericMonsterDestructable : public destructable
{
public:
    genericMonsterDestructable(unsigned maxHp, int physDef, int menDef, int xp, int dodge = 0, int deadChar = '%', std::string deadName = "");
    void die(object* owner);
};
#endif // DESTRUCTABLE_H
