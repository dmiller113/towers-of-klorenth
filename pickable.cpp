//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//pickable.cpp

//Includes
#include "pickable.h"
#include "defines.h"
#include "gameEngine.h"
#include "messageQueue.h"

//Defines


//Globals


//Enums


//Classes


//Functions



pickable::pickable(std::string fakeName, effect* effect, TargetSelector* selector, bool id, unsigned charges, bool perm) :
    _selector(selector), _effect(effect), _isIdentified(id), _falseName(fakeName), _charges(charges), _permanent(perm), _wearable(NULL)
{
    //ctor
}

pickable::~pickable()
{
    //dtor
    if(_effect)
        delete _effect;
    if(_selector)
        delete _selector;
}

bool pickable::use(object* owner, object* picker)
{
    std::vector<object*> effectedObjects;
    bool success = false;
    if(_charges > 0 && _effect != NULL)
    {
        if(_selector)
        {
            _selector->selectTargets(picker, effectedObjects);
        }else
            effectedObjects.push_back(picker);
        for(unsigned i = 0; i < effectedObjects.size(); i++)
        {
            if(_effect)
            {
                if( _effect->applyTo(effectedObjects[i], owner) )
                {
                    success = true;
                }
            }
        }
    }
    if(_permanent != true)
    {
        _charges -= 1;
        if(_charges == 0)
        {
            picker->getContainer()->deleteItem(owner);
        }

    }
    return success;
}

bool pickable::pick(object* owner, object* picker)
{
    //check to see that the caller has a container with room
    if(picker->getContainer())
    {
        if(picker->getContainer()->addItem(owner))
        {
            game->removeObject(owner, false);
            return true;
        }
    }
    return false;
}

void pickable::drop(object* owner, object* picker)
{
    if(picker->getContainer())
    {
        int tempPos[2];
        picker->getPos(tempPos);
        picker->getContainer()->removeItem(owner);
        owner->setPos(tempPos);
        game->getList().push_back(owner);
        game->sendToFront(owner);
        if(owner->getPickable()->_isIdentified)
            messager.addMessage(picker->getName() + " dropped a/an " + owner->getName() + "." );
        else
            messager.addMessage(picker->getName() + " dropped a/an " + _falseName + "." );
    }
}
