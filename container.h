#ifndef CONTAINER_H
#define CONTAINER_H


//Towers of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//container.h

//Includes
#include <vector>
#include "object.h"

//Defines


//Globals


//Enums


//Classes



class container
{
    std::vector<object*> _itemList;
    unsigned _maxSize;
public:
    container(unsigned size = 26) : _maxSize(size) { ; }
    ~container();
    bool addItem(object* item);
    object* getItem(unsigned pos);
    bool removeItem(unsigned pos);
    bool removeItem(object* item);
    bool deleteItem(unsigned pos);
    bool deleteItem(object* item);
    std::vector<object*>& getItemList(void) {return _itemList;}
    unsigned getSize(void) {return _itemList.size();}
};

#endif // CONTAINER_H
