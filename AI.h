//Towers of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//AI.h

#ifndef AI_H
#define AI_H

//Includes
#include "object.h"
#include "map.h"
#include "defines.h"
//Defines


//Globals


//Enums


//Classes
class object;

//Functions



class AI
{
protected:
    int _energy;
    int _stamina;
    int _energyRecharge;
public:
    AI(int eCharge) : _energyRecharge(eCharge) {_energy = 0; _stamina = 100;}
    virtual ~AI();
    virtual void update(object* owner, gameState& gs, map* currentMap) = 0;
    int getStamina( void ) {return _stamina;}
    bool changeStamina( int cValue );
    bool changeEnergy(int cValue );
    bool energyTick( void );
    int getEnergy( void ) { return _energy; }
    float calcLineDistance(float x1, float y1, float x2, float y2);
};

class playerAI : public AI
{
public:
    playerAI( int eCharge = 30 ) : AI(eCharge) {;}
    void update(object* owner, gameState& gs, map* currentMap);
    bool moveOrAttack(object* owner, map* currentMap, int cX, int cY);
    void displayDiscription(object* item);
};

class genericMonsterAI : public AI
{
    int _sightRadius;
    int _chaseTurns;
    int _maxChaseTurns;
    int _lastPPos[2];
    bool _hasSeen;
public:
    genericMonsterAI(int sightRadius, int chaseTurns, int eCharge = 30);
    void update(object* owner, gameState& gs, map* currentMap);
    void moveOrAttack(object* owner, map* currentMap, int cX, int cY);
    void wander(object* owner, map* currentMap);
};
#endif // AI_H
