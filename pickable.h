#ifndef PICKABLE_H
#define PICKABLE_H

//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//pickable.h

//Includes
#include "effect.h"
#include "TargetSelector.h"
#include "wearable.h"
#include <string>
//Defines


//Globals


//Enums


//Classes
class effect;
class TargetSelector;
class wearable;

class pickable
{
protected:
    TargetSelector* _selector;
    effect* _effect;
    bool _isIdentified;
    std::string _falseName;
    unsigned _charges;
    bool _permanent;
public:
    wearable* _wearable;
    pickable(std::string fakeName, effect* effect, TargetSelector* selector = NULL, bool id = false, unsigned charges = 1, bool perm = false);
    virtual ~pickable();
    bool pick(object* owner, object* picker);
    void drop(object* owner, object* picker);
    bool use(object* owner, object* picker);
    bool isID(void) {return _isIdentified;}
    std::string getFakeName(void) {return _falseName;}
    bool hasEffect(void) {if(_effect != NULL) return true; else return false;}
};

#endif // PICKABLE_H
