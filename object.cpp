//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//object.cpp

//Includes
#include "object.h"
#include <iostream>
#include <sstream>
#include "gameEngine.h"

//Defines


//Globals


//Enums


//Classes


//Functions
int convertStoI(std::string iValue)
{
    int Number;
    if ( ! (std::istringstream(iValue) >> Number) ) Number = 0;
    return Number;
}

float convertStoF(std::string fValue)
{
    float Number;
    if ( ! (std::istringstream(fValue) >> Number) ) Number = 0.0;
    return Number;
}

TCODColor convertColor(std::string color)
{
    //need to convert strings to colors
    if(color == "black")
        return TCODColor::black;
    if(color == "white")
        return TCODColor::white;
    if(color == "red")
        return TCODColor::red;
    if(color == "brass")
        return TCODColor::brass;
    if(color == "green")
        return TCODColor::green;
    if(color == "sky")
        return TCODColor::sky;
    if(color == "orange")
        return TCODColor::orange;
    if(color == "blue")
        return TCODColor::blue;
    if(color == "azure")
        return TCODColor::azure;
    if(color == "yellow")
        return TCODColor::yellow;
    if(color == "han")
        return TCODColor::han;
    if(color == "grey")
        return TCODColor::grey;
    if(color == "amber")
        return TCODColor::amber;
    if(color == "brown")
        return TCODColor::sepia;
    if(color == "cyan")
        return TCODColor::cyan;
    if(color == "crimson")
        return TCODColor::crimson;
    if(color == "potion" || color == "wand" || color == "rod")
        return TCODColor::darkestSepia;
    return TCODColor::white;
}

equipLocations convertStoLocation(std::string term)
{
    if(term == "weapon")
        return EQUIPWEAPON;
    if(term == "body")
        return EQUIPBODY;
    if(term == "ring")
        return EQUIPRING;
    if(term == "neck")
        return EQUIPNECK;
    if(term == "misc")
        return EQUIPMISC;
    else
        return EQUIPNONE;
}

ability* convertStoAbility(std::string term)
{
    if(term != "none")
    {
        ability* tempAbility = new ability;
        effect* tempEffect = NULL;
        TargetSelector* tempTarget = NULL;
        int amount = 0;
        float range = 0.0;
        std::string aMessage;
        //grab what kind of effect it is
        std::string tempString = term.substr(0, term.find_first_of('(') );
        term = term.substr(term.find_first_of('(')+1);
        std::string targetType = term.substr(0, term.find_first_of(')') );
        term = term.substr(term.find_first_of('(')+1);
        if(targetType != "self")
        {
            range = convertStoF( term.substr(0, term.find_first_of(')') ) ) ;
            term = term.substr(term.find_first_of('(')+1);
        }else
        {
            range = 0;
        }
        amount = convertStoI( term.substr(0, term.find_first_of(')') ) ) ;
        term = term.substr(term.find_first_of(':')+1);
        //grab the message
        aMessage = term;

        //grabbed all the parts, now assemble the ability.
        ;
        //first, find out what type of effect we need.
        if(tempString == "heal")
        {
            tempEffect = new changeHealthEffect((-1*amount), DMGTRUE, aMessage);
        }else if(tempString == "pdamage")
        {
            tempEffect = new changeHealthEffect(amount, DMGPHYSICAL, aMessage);
        }
        else if(tempString == "mdamage")
        {
            tempEffect = new changeHealthEffect(amount, DMGMENTAL, aMessage);
        }
        else if(tempString == "tdamage")
        {
            tempEffect = new changeHealthEffect(amount, DMGTRUE, aMessage);
        }else if(tempString == "light")
        {
            tempEffect = new changeLightRadiusEffect(amount, aMessage);
        }
        //then, make the targetSelector
        if(targetType != "self")
        {
            if(targetType == "closestMonster")
            {
                tempTarget = new TargetSelector(CLOSEST_MONSTER, range);
            }else if(targetType == "targetTile")
            {
                tempTarget = new TargetSelector(SELECTED_TILE, range);
            }else if(targetType == "targetMonster")
            {
                tempTarget = new TargetSelector(SELECTED_MONSTER, range);
            }else if(targetType == "pbaoe")
            {
                tempTarget = new TargetSelector(PBAOE, range);
            }
        }
        //Already got the range, just assemble the ability and ship back to factory.
        tempAbility->selector = tempTarget;
        tempAbility->range = range;
        tempAbility->apply = tempEffect;
        tempAbility->type = term;
        return tempAbility;
    }else
    return NULL;
}

object::object(std::string name, int x, int y, int character, const TCODColor& back, const TCODColor& fore, bool blocks, std::string desc) :
   _name(name), _description(desc), _xPos(x), _yPos(y), _character(character), _backColor(back), _foreColor(fore), _isBlocking(blocks),
   _destroyable(NULL), _damager(NULL), _inventory(NULL), _equipped(NULL), _item(NULL), _AI(NULL), _traits(NULL)
{
    //ctor
    ;//All the composite objects get called to null here.

}

object::~object()
{
    //dtor
    ;//need to deallocate all the composite objects here.
    if(_destroyable)
        delete _destroyable;
    if(_AI)
        delete _AI;
    if(_damager)
        delete _damager;
    if(_inventory)
        delete _inventory;
    if(_item)
        delete _item;
}

void object::draw(TCODConsole* con)
{
    con->putCharEx(_xPos, _yPos, _character, _foreColor, _backColor);
    return;
}

void object::addTraits(specialTraits* trait)
{
    if( _traits)
        delete _traits;
    _traits = trait;
}

void object::addAI(AI* newAI)
{
    if(_AI != NULL)
    {
        delete _AI;
    }
    _AI = newAI;
}

void object::addInventory(container* inventory)
{
    if(_inventory != NULL)
    {
        delete _inventory;
    }
    _inventory = inventory;
}

void object::getPos(int Pos[2])
{
    Pos[0] = _xPos;
    Pos[1] = _yPos;
    return;
}

void object::movePos(int cX, int cY)
{
    _xPos += cX;
    _yPos += cY;
    return;
}

void object::addDamager(damager* newDamager)
{
    if(_damager != NULL)
    {
        delete _damager;
    }
    _damager = newDamager;
}

void object::addDestructable(destructable* newDestroyable)
{
    if(_destroyable != NULL)
    {
        delete _destroyable;
    }
    _destroyable = newDestroyable;
}

void object::addPickable(pickable* newPickable)
{
    if(_item != NULL)
    {
        delete _item;
    }
    _item = newPickable;
}

void object::addEquip(container* newEquip)
{
    if(_equipped != NULL)
    {
        delete _equipped;
    }
    _equipped = newEquip;
    _equipped->getItemList().resize(5, NULL);

}

void object::setPos(int pos[2])
{
    if(pos[0] < MAPMAXX && pos[1] < MAPMAXY)
    {
        _xPos = pos[0];
        _yPos = pos[1];
    }
}

monsterFactory::monsterFactory(std::string filePath)
{
    std::fstream file;
    file.open(filePath.c_str(), std::ifstream::in);
    std::string entry;
    std::string temp;
    monsterTraits tempMonster;
    char tempString[256];
    bool isGood = true;
    if(!file.is_open())
    {
        std::cout<<"Error in opening file"<<std::endl;
        return;
    }else
    {
        while( isGood )
        {
            file.getline(tempString, 256, '#');
            entry = tempString;
            if(entry[0] != '&')
            {
                tempMonster.symbol = entry[0];
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.name = entry.substr(0, entry.find_first_of(':',0) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.backColor = convertColor(entry.substr(0, entry.find_first_of(':',0) ) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.foreColor = convertColor(entry.substr(0, entry.find_first_of(':',0) ) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.description = entry.substr(0, entry.find_first_of(':',0));
                entry = entry.substr(entry.find_first_of(':',0)+1);
                if(entry.substr(0, entry.find_first_of(':',0)) == "t")
                    tempMonster.blocks = true;
                else
                    tempMonster.blocks = false;
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.strength = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.dex = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.will = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.maxHP = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.dodge = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.pdef = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.mdef = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.patk = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.matk = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.sightRadius = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.chaseTurns = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.energyCharge = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );
                entry = entry.substr(entry.find_first_of(':',0)+1);
                tempMonster.xp = convertStoI(entry.substr(0, entry.find_first_of(':',0)) );

                monsterDefs.push_back(tempMonster);


            }
            file.ignore(1);
            isGood = file.good();
        }
        file.good();
    }

}

object* monsterFactory::createMonster(int symbol, int xPos, int yPos,
        int strength, int dex, int will, int maxHP, int dodge, int pdef,
        int mdef, int patk, int matk, int sightRadius, int chaseTurns, int energyCharge)
{
    ;//returns a null if unable to construct.
    //std::cout<<"in second createMonster"<<std::endl;
    object* tempObject = NULL;
    object* tempEquip = NULL;
    if(game->getMap()->getTile(xPos, yPos)->isWalkable)
    {
        for(unsigned i = 0; i < monsterDefs.size(); i++)
        {
            //std::cout<<"in for loop"<<std::endl;
            if(symbol == monsterDefs[i].symbol)
            {
                //std::cout<<"symbol matches"<<std::endl;
                //right monster definition. Make it!
                tempObject = new object(monsterDefs[i].name, xPos, yPos, symbol, monsterDefs[i].backColor,
                                        monsterDefs[i].foreColor, monsterDefs[i].blocks, monsterDefs[i].description);
               /* std::cout<<"monsterDef Name = "<<monsterDefs[i].name<<std::endl;
                std::cout<<"base"<<std::endl;*/
                tempObject->addAI(new genericMonsterAI(sightRadius, chaseTurns, energyCharge) );
                //std::cout<<"ai"<<std::endl;
                tempObject->addDamager(new damager(strength, dex, will ));
                //std::cout<<"damage"<<std::endl;
                tempObject->addDestructable(new genericMonsterDestructable(maxHP, pdef, mdef, monsterDefs[i].xp, dodge));
                //std::cout<<"destruction"<<std::endl;
                //std::cout<<"tempObject name= "<<tempObject->getName().c_str()<<std::endl;
                tempObject->addEquip(new container(1));
                tempEquip = new object("monsterStats",0,0,',',TCODColor::black,TCODColor::black, false);
                tempEquip->addPickable(new pickable("monster",NULL,NULL));
                tempEquip->getPickable()->_wearable = new wearable(EQUIPWEAPON, monsterDefs[i].patk, 0,
                                                                    monsterDefs[i].matk);
                tempObject->getEquip()->addItem(tempEquip);
                break;
            }
        }

    }
    //std::cout<<"end second createMonster"<<std::endl;
    //if(tempObject != NULL)
        //std::cout<<tempObject->getName()<<std::endl;
    return tempObject;
}

object* monsterFactory::createMonster(int symbol, int xPos, int yPos)
{
    //std::cout<<"in first createMonster"<<std::endl;
    object* tempObject = NULL;
    for(unsigned i = 0; i < monsterDefs.size(); i++)
    {
        if(symbol == monsterDefs[i].symbol)
        {
            tempObject = createMonster(symbol, xPos, yPos, monsterDefs[i].strength, monsterDefs[i].dex,
                monsterDefs[i].will, monsterDefs[i].maxHP, monsterDefs[i].dodge, monsterDefs[i].pdef, monsterDefs[i].mdef,
                monsterDefs[i].patk, monsterDefs[i].matk, monsterDefs[i].sightRadius, monsterDefs[i].chaseTurns, monsterDefs[i].energyCharge );
            break;
        }
    }
    return tempObject;
}

object* monsterFactory::createMonster(std::string name, int xPos, int yPos)
{
    object* tempObject = NULL;
    for(unsigned i = 0; i < monsterDefs.size(); i++)
    {
        if(name == monsterDefs[i].name)
        {
            tempObject = createMonster(monsterDefs[i].symbol, xPos, yPos, monsterDefs[i].strength, monsterDefs[i].dex,
                monsterDefs[i].will, monsterDefs[i].maxHP, monsterDefs[i].dodge, monsterDefs[i].pdef, monsterDefs[i].mdef,
                monsterDefs[i].patk, monsterDefs[i].matk, monsterDefs[i].sightRadius, monsterDefs[i].chaseTurns, monsterDefs[i].energyCharge );
            break;
        }
    }
    return tempObject;

}

itemFactory::itemFactory(std::string filePath)
{
    std::fstream file;
    file.open(filePath.c_str(), std::ifstream::in);
    std::string entry;
    std::string preAbilities;
    std::string temp;
    itemTraits tempItem;
    char tempString[256];
    bool isGood = true;
    if(!file.is_open())
    {
        std::cout<<"Error in opening item file"<<std::endl;
        return;
    }else
    {
        while( isGood )
        {
            file.getline(tempString, 256, '#');
            entry = tempString;
            if(entry[0] != '&')
            {
                preAbilities = entry.substr(0,entry.find_first_of('|',0));
                entry = entry.substr(entry.find_first_of('|',0)+1);
                //grab symbol
                tempItem.symbol = preAbilities[0];
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab Real name
                tempItem.realName = preAbilities.substr(0, preAbilities.find_first_of(':'));
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab description
                tempItem.description = preAbilities.substr(0, preAbilities.find_first_of(':'));
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab fake name
                tempItem.fakeName = preAbilities.substr(0, preAbilities.find_first_of(':'));
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab the fore color
                tempItem.foreColor = convertColor(preAbilities.substr(0, preAbilities.find_first_of(':',0) ) );
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab the background color;
                tempItem.backColor = convertColor(preAbilities.substr(0, preAbilities.find_first_of(':',0) ) );
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab if equipment
                if( preAbilities.substr(0, preAbilities.find_first_of(':')) == "true")
                    tempItem.isEquip = true;
                else
                    tempItem.isEquip = false;
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab if permanent
                if( preAbilities.substr(0, preAbilities.find_first_of(':')) == "true")
                    tempItem.isPerm = true;
                else
                    tempItem.isPerm = false;
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab dodge
                tempItem.dodge = convertStoI( preAbilities.substr(0, preAbilities.find_first_of(':')) );
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab physical attack
                tempItem.patk = convertStoI( preAbilities.substr(0, preAbilities.find_first_of(':')) );
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab mental attack
                tempItem.matk = convertStoI( preAbilities.substr(0, preAbilities.find_first_of(':')) );
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab physical defense
                tempItem.pdef = convertStoI( preAbilities.substr(0, preAbilities.find_first_of(':')) );
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab mental defense
                tempItem.mdef = convertStoI( preAbilities.substr(0, preAbilities.find_first_of(':')) );
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab sightradius
                tempItem.sightRadius = convertStoI( preAbilities.substr(0, preAbilities.find_first_of(':')) );
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab charges
                tempItem.charges = convertStoI( preAbilities.substr(0, preAbilities.find_first_of(':')) );
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field
                //grab type of item. consume for 1 time use, wand for nonperm charged item, rod for perm charged
                //item, and then weapon body ring neck misc for equipment.
                if(tempItem.isEquip)
                    tempItem.location = convertStoLocation(preAbilities.substr(0, preAbilities.find_first_of(':')));
                else
                    tempItem.location = EQUIPNONE;
                preAbilities = preAbilities.substr(preAbilities.find_first_of(':', 0)+1); //Advance to the next field

                //handle getting the ID game shit
                ;
                //tempshit just to get it there.
                tempItem.isID = true; //THIS IS TEMP. FIX AFTER STUFF IS DONE.

                //non ability portion is done.

                //handle abilities
                //set up default to NULL
                tempItem.onAttack = NULL;
                tempItem.onBAttack = NULL;
                tempItem.onHit = NULL;
                tempItem.onBHit = NULL;
                tempItem.onEquip = NULL;
                tempItem.onRemove = NULL;
                tempItem.onUse = NULL;

                //Then fill in the abilities that are there.
                if(entry.find_first_of('|') != std::string::npos )
                {
                    tempItem.onUse = convertStoAbility(entry.substr(0, entry.find_first_of('|')));
                    entry = entry.substr(entry.find_first_of('|')+1);
                }
                if(entry.find_first_of('|') != std::string::npos)
                {
                    tempItem.onAttack = convertStoAbility(entry.substr(0, entry.find_first_of('|')));
                    entry = entry.substr(entry.find_first_of('|')+1);
                }
                if(entry.find_first_of('|') != std::string::npos)
                {
                    tempItem.onHit = convertStoAbility(entry.substr(0, entry.find_first_of('|')));
                    entry = entry.substr(entry.find_first_of('|')+1);
                }
                if(entry.find_first_of('|') != std::string::npos)
                {
                    tempItem.onBAttack = convertStoAbility(entry.substr(0, entry.find_first_of('|')));
                    entry = entry.substr(entry.find_first_of('|')+1);
                }
                if(entry.find_first_of('|') != std::string::npos)
                {
                    tempItem.onBHit = convertStoAbility(entry.substr(0, entry.find_first_of('|')));
                    entry = entry.substr(entry.find_first_of('|')+1);
                }
                if(entry.find_first_of('|') != std::string::npos)
                {
                    tempItem.onEquip = convertStoAbility(entry.substr(0, entry.find_first_of('|')));
                    entry = entry.substr(entry.find_first_of('|')+1);
                }
                if(entry.find_first_of('|') != std::string::npos)
                {
                    tempItem.onRemove = convertStoAbility(entry.substr(0, entry.find_first_of('|')));
                    entry = entry.substr(entry.find_first_of('|')+1);
                }


                //Push the new definition on to the vector of item defs
                itemDefs.push_back(tempItem);

                file.ignore(1);
                isGood = file.good();
            }else
            {
                isGood = false;
            }

        }
    }

}

object* itemFactory::makeItem(int x, int y, std::string realName, int charges)
{
    unsigned pos = 0;
    bool success = false;
    object* tempObject = NULL;
    pickable* tempPickable = NULL;
    wearable* tempWear = NULL;
    TargetSelector* tempTarget = NULL;

    //find out which item we're making;
    for(unsigned i = 0; i < itemDefs.size(); i++)
    {
        if(itemDefs[i].realName == realName)
        {
            //we've found it!
            success = true;
            pos = i;
        }
    }

    //either we've got the item, or no. If no, return a null.
    if(!success)
        return tempObject; //Not found. Get out early and return NULL.
    else
    {
        //ho snaps we found it.
        //construct the parts
        tempObject = new object(realName, x, y, itemDefs[pos].symbol, itemDefs[pos].backColor,
            itemDefs[pos].foreColor, false, itemDefs[pos].description);
        if(itemDefs[pos].onUse)
        {
            if(itemDefs[pos].onUse->selector)
                tempTarget = new TargetSelector(itemDefs[pos].onUse->selector->getType(), itemDefs[pos].onUse->selector->getRange());
            if(charges == -1)
            {
                tempPickable = new pickable(itemDefs[pos].fakeName, itemDefs[pos].onUse->apply->getCopy(),
                                    tempTarget, false, itemDefs[pos].charges, itemDefs[pos].isPerm);
            }else
            {
                tempPickable = new pickable(itemDefs[pos].fakeName, itemDefs[pos].onUse->apply->getCopy(),
                                        tempTarget, false, charges, itemDefs[pos].isPerm);
            }
        }else
            tempPickable = new pickable(itemDefs[pos].fakeName, NULL, NULL, false, itemDefs[pos].charges, itemDefs[pos].isPerm);
        if(itemDefs[pos].isEquip)
        {
            ability* tOnHit = NULL;
            ability* tOnAttack = NULL;
            ability* tOnBHit = NULL;
            ability* tOnBAttack = NULL;
            ability* tOnEquip = NULL;
            ability* tOnRemove = NULL;
            //Assemble the abilities that are actually there.
            if(itemDefs[pos].onHit != NULL)
                tOnHit = new ability(itemDefs[pos].onHit);
            if(itemDefs[pos].onAttack != NULL)
                tOnAttack = new ability(itemDefs[pos].onAttack);
            if(itemDefs[pos].onBHit != NULL)
                tOnBHit = new ability(itemDefs[pos].onBHit);
            if(itemDefs[pos].onBAttack != NULL)
                tOnBAttack = new ability(itemDefs[pos].onBAttack);
            if(itemDefs[pos].onEquip != NULL)
                tOnEquip = new ability(itemDefs[pos].onEquip);
            if(itemDefs[pos].onRemove != NULL)
                tOnRemove = new ability(itemDefs[pos].onRemove);

            tempWear = new wearable(itemDefs[pos].location, itemDefs[pos].patk, itemDefs[pos].pdef, itemDefs[pos].matk,
                                    itemDefs[pos].mdef, itemDefs[pos].dodge, tOnHit, tOnAttack, tOnBAttack, tOnBHit, tOnEquip, tOnRemove );
        }
        //assemble the parts
        tempObject->addPickable(tempPickable);
        tempObject->getPickable()->_wearable = tempWear;
        return tempObject;
    }


    ;
}


bool itemFactory::randomizeIDs( void )
{
    TCODRandom* rand = new TCODRandom;

}
