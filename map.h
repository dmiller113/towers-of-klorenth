//Towers of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//map.h


#ifndef MAP_H
#define MAP_H

//Includes
#include "libtcod.hpp"
#include "defines.h"
#include <string>
#include <vector>


//Defines
static const int MAXMONSTERDEFS = 21;
static const int MAXITEMDEFS = 20;

//Globals


//Enums


//Classes

struct spawnChance
{
    unsigned minValue;
    unsigned maxValue;
    std::string name;
    unsigned baseChance;
    float valueScale;
    spawnChance();
    spawnChance(unsigned mV, unsigned maV, std::string na, unsigned bC, float vS);
    unsigned getChance(int dLevel);
};

class spawnChart
{
    std::vector<spawnChance> _monsterChances;
    std::vector<spawnChance> _itemChances;
public:
    spawnChart(spawnChance monsterSpawns[MAXMONSTERDEFS], spawnChance itemSpawns[MAXITEMDEFS]);
    int getMaxChance(int dlevel, bool monster = true);
    std::string getMonsterSpawn(int rand, int dlevel);
    std::string getItemSpawn(int rand, int dlevel);
};


struct tile
{
    int _symbol;
    TCODColor _backColor;
    TCODColor _foreColor;
    bool isWalkable;
    bool isTransparent;
    bool isExplored;
    //step on effect
};

class map
{
protected:
    tile _map[MAPMAXX][MAPMAXY];
    TCODRandom* rand;
    friend class BspListener;
    void dig(int x1, int y1, int x2, int y2);
    void createRoom(bool first, int x1, int y1, int x2, int y2, bool& isStairs);
public:
    map();
    map(int seed);
    ~map();
    bool populateMap(void);
    tile* getTile(unsigned x, unsigned y);
    bool isWalkable(int x, int y);

};

#endif // MAP_H
