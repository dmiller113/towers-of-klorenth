//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//gameEngine.cpp

//Includes
#include "gameEngine.h"
#include <iostream>
#include "messageQueue.h"
#include "graphics.h"

//Defines


//Globals
const bool debug = true;
std::vector<std::string> levelUpChoices;
//Enums


//Classes


//Functions


gameEngine::gameEngine(int seed) :
    _currentMap(NULL), _player(NULL), _dlevel(1)
{

    //ctor
    //temp
    int oldPXP;
    int oldPLvl;
    if(seed == 0)
    {
        TCODRandom* rand = TCODRandom::getInstance();
        _seed = rand->getInt(1, 9999999);
        oldPXP = 0;
        oldPLvl = 1;
    }else
    {
        _seed = seed;
        //load shit.
    }
    init(seed);
    _playerLevel = oldPLvl;
    _playerXP = oldPXP;
    levelUpChoices.push_back("Strength");
    levelUpChoices.push_back("Dexterity");
    levelUpChoices.push_back("Will");

}

int gameEngine::gainXP(int amount)
{
    _playerXP+= amount;
    return amount;
}

void gameEngine::init(int seed)
{
    //make the player
    _player = new object("Player",0, 0, '@', TCODColor::black, TCODColor::white, true);
    _player->addDestructable(new playerDestructable(20, 5, 5, 0));
    _player->addDamager(new damager(3, 3, 3));
    _player->addAI(new playerAI());
    _player->addInventory(new container());
    _player->addEquip(new container(5));
    _playerXP = 0;
    _objectList.push_back(_player);
}

gameEngine::~gameEngine()
{
    //dtor
    if(_currentMap != NULL)
    {
        delete _currentMap;
    }
    for(unsigned i = 0; i < _objectList.size(); i++)
    {
        if(_objectList[i] != NULL)
        {
            delete _objectList[i];
        }
    }
    if(_player != NULL)
    {
        delete _player;
    }
}

object* gameEngine::getPlayer( void )
{
    return _player;
}

map* gameEngine::getMap( void )
{
    //std::cout<<"in getMap()"<<std::endl;
    //std::cout<<"_currentMap = "<<std::endl;
    return _currentMap;
}

void gameEngine::tick(gameState& gs)
{
    object* tempObject = NULL;
    _player->getDamager()->calcAttacks(_player);
    static int turnCount = 0;


    for(unsigned i = 0; i < _objectList.size(); i++)
    {
        tempObject = _objectList[i];
        if(_objectList[i]->getAI() != NULL)
        {
            if(gs != GAMEPLAYERACT && !_player->getDestroyable()->isDead())
                _objectList[i]->getAI()->energyTick();
            if(gs != GAMEPLAYERACT || _objectList[i] == _player)
                _objectList[i]->getAI()->update(_objectList[i], gs, _currentMap);

        }
    }


}

void gameEngine::createMap()
{
    //Function to create a new map. Deallocates the old map if it exists.
    //std::cout<<"in createMap"<<std::endl;
    if(_currentMap != NULL)
    {
        delete _currentMap;
    }
    //seed not currently used, will be passed to the map constructor for making new levels.
    if(debug && _seed == 1113)
        _currentMap = new map();
    else
    {
        _currentMap = new map(_seed);
    }
    //std::cout<<"_currentMap = "<<_currentMap<<std::endl;
    return;
}

object* gameEngine::getObject(unsigned pos)
{
    //returns the object at pos in the vector, if pos exists
    if(pos < _objectList.size() && pos >= 0)
    {
        return _objectList[pos];
    }else
    {
        //throw an error or something.
        return _player;
    }
}

void gameEngine::removeObject(unsigned pos, bool isDeleted)
{
    //removes an object from the vector
    if(pos < _objectList.size() && pos >= 0)
    {
        if(isDeleted)
        {
            if(_objectList[pos] != NULL)
            {
                delete _objectList[pos];
            }
        }
        _objectList.erase( (_objectList.begin() + pos) );
    }else
    {
        return;
    }
}

void gameEngine::removeObject(object* item, bool isDeleted)
{
    //removes an object from the vector
    unsigned pos = 0;
    bool found = false;
    if(item)
    {
        for(unsigned i = 0; i < _objectList.size(); i++)
        {
            if(item == _objectList[i])
            {
                pos = i;
                found = true;
                break;
            }
        }
        if(found)
        {
            if(isDeleted)
            {
                if(_objectList[pos] != NULL)
                {
                    delete _objectList[pos];
                }
            }
            _objectList.erase( (_objectList.begin() + pos) );
        }
    }else
    {
        return;
    }
}

void gameEngine::sendToFront(unsigned pos)
{
    //send the object at pos to the front, i.e. [0]
    object* tempPointer = _objectList[pos];
    removeObject(pos, false);
    _objectList.insert(_objectList.begin(), tempPointer);
    return;
}

void gameEngine::sendToFront(object* target)
{
    for(unsigned i = 0; i < _objectList.size(); i++)
    {
        if(_objectList[i] == target)
        {
            removeObject(i, false);
            _objectList.insert(_objectList.begin(), target);
            return;
        }

    }
}

void gameEngine::nextLevel( void )
{
    TCODRandom* rand = TCODRandom::getInstance();
    _dlevel += 1;
    _seed = rand->getInt(0, 9999999);

    for(int i = _objectList.size() -1 ; i >= 0 ; i--)
    {
        if(_objectList[i] != _player)
        {
            delete _objectList[i];

        }
        _objectList.pop_back();
    }
    _objectList.push_back(_player);
    createMap();
    _currentMap->populateMap();
}

int gameEngine::returnNextLevel(void)
{
    int tempExp = 0;
    int lastExp = 0;
    int tXP = 0;

    if(_playerLevel == 1)
        tempExp = 100;
    if(_playerLevel == 2)
        tempExp = 200;
    if(_playerLevel >= 3)
    {
        tempExp = 400;
        lastExp = 200;
        for(unsigned i = 3; i <= _playerLevel; i++ )
        {
            tXP = tempExp;
            tempExp += tempExp + lastExp;
            lastExp = tXP;

        }
    }
    return tempExp;
}

void gameEngine::checkLevel(void)
{
    int tempExp = returnNextLevel();
    int choice = 0;
    if(_playerXP >= tempExp)
    {
        messager.addMessage("You've leveled up!");
        _playerLevel += 1;
        do
        {
            choice = graphicEngine.drawSelectionWindow("Choose a stat to improve:", levelUpChoices);
        }while(choice == BADKEY);
        switch(choice)
        {
            case 0:
            {
                _player->getDamager()->changeStr(1);
                break;
            }
            case 1:
            {
                _player->getDamager()->changeDex(1);
                break;
            }
            case 2:
            {
                _player->getDamager()->changeWill(1);
                break;
            }
        }
        _player->getDestroyable()->changeMaxHP(10);

        //next display the gain a feat screen
        ;
    }
}

object* gameEngine::getClosestMonster(int x, int y, float range)
{
    object* closest = NULL;
    float bestDistance = 99999.0;
    int tempPos[2];
    for (unsigned i = 0; i < _objectList.size(); i++)
    {
        object* actor = _objectList[i];
        if ( actor != _player && actor->getDestroyable() )
        {
            actor->getPos(tempPos);
            float distance = getDistance(x, y, tempPos[0], tempPos[1]);
            if ( distance < bestDistance && ( distance <= range || range == 0.0f ) )
            {
                bestDistance = distance;
                closest = actor;
            }
        }
    }
    return closest;
}

bool gameEngine::pickATile(int& x, int& y, float range)
{
    int tempPos[2];
    int selectX, selectY;
    _player->getPos(tempPos);
    selectX = tempPos[0];
    selectY = tempPos[1];
    graphicEngine.render(*_player, *_currentMap, _objectList);
    while(!TCODConsole::isWindowClosed() )
    {
        //
        for(int cX = 0; cX < MAPMAXX; cX++)
        {
            for(int cY = 0; cY < MAPMAXY; cY++)
            {
                if(graphicEngine.getFov()->isInFov(cX, cY) && (range == 0.0 || getDistance(tempPos[0], tempPos[1], cX, cY) <= range ) )
                {
                    TCODColor col = TCODConsole::root->getCharBackground(cX,cY);
                    col = TCODColor::darkestGrey;
                    TCODConsole::root->setCharBackground(cX+1, cY+1, col);
                }
            }
        }
        if(graphicEngine.getFov()->isInFov(selectX, selectY) && (range == 0.0 || getDistance(tempPos[0], tempPos[1], selectX, selectY) <= range ) )
            TCODConsole::root->setCharBackground(selectX + 1, selectY + 1, TCODColor::amber);
        TCOD_key_t key = TCODConsole::checkForKeypress(TCOD_KEY_PRESSED);
        if(key.c == '4' || key.vk == TCODK_LEFT)
        {
            selectX += -1;
            selectY += 0;
        }
        if(key.c == '6' || key.vk == TCODK_RIGHT)
        {
            selectX += 1;
            selectY += 0;
        }
        if(key.c == '8' || key.vk == TCODK_UP)
        {
            selectX += 0;
            selectY += -1;
        }
        if(key.c == '2' || key.vk == TCODK_DOWN)
        {
            selectX += 0;
            selectY += 1;
        }
        if(key.c == '7')
        {
            selectX += -1;
            selectY += -1;
        }
        if(key.c == '9')
        {
            selectX += 1;
            selectY += -1;
        }
        if(key.c == '1')
        {
            selectX += -1;
            selectY += 1;
        }
        if(key.c == '3')
        {
            selectX += 1;
            selectY += 1;
        }
        if(key.vk == TCODK_ENTER || key.vk == TCODK_KPENTER)
        {
            x = selectX;
            y = selectY;
            return true;
        }
        if(key.vk == TCODK_ESCAPE)
        {
            return false;
        }
    TCODConsole::flush();
    }
    return false;
}

object* gameEngine::getObject(int x, int y)
{
    int tempPos[2];
    for(unsigned i = 0; i < _objectList.size(); i++)
    {
        _objectList[i]->getPos(tempPos);
        if(tempPos[0] == x && tempPos[1] == y && (_objectList[i]->getDestroyable()))
            return _objectList[i];
    }
    return NULL;
}
