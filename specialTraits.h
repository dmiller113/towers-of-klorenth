#ifndef SPECIALTRAITS_H
#define SPECIALTRAITS_H
//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//specialTraits.h

//Includes
#include "libtcod.hpp"
#include "wearable.h"
#include "object.h"
#include <string>
#include <vector>

//Defines


//Globals


//Enums


//Classes
struct ability;


struct spell
{
    ability* _ability;
    std::string _name;
    ~spell()    {if(_ability) delete _ability;}
};


class specialTraits
{
public:
    std::vector<spell*> spellBook;
    ability* _onHit;
    ability* _onAttack;
    ability* _onBAttack;
    ability* _onBHit;
    ability* _onUpkeep;
    specialTraits(std::vector<spell> spellBook, ability* onHit = NULL, ability* onAttack = NULL,
        ability* onBAttack = NULL, ability* onBHit = NULL, ability* onUpkeep = NULL );
    ~specialTraits();
protected:
};

#endif // SPECIALTRAITS_H
