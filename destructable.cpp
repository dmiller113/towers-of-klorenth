//Towers of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//destructable.cpp

//Includes
#include "destructable.h"
#include "messageQueue.h"
#include <sstream>
#include "gameEngine.h"

//Defines


//Globals


//Enums


//Classes


//Functions



destructable::destructable(unsigned maxHP, int physDef, int menDef, int xp, int dodge, int deadChar, std::string deadName):
    _maxHp(maxHP), _currentHP(maxHP), _physicalDef(physDef), _mentalDef(menDef), _worthXP(xp), _dodge(dodge), _deadSymbol(deadChar), _deadName(deadName)
{
    //ctor
}

destructable::~destructable()
{
    //dtor
}

void destructable::die(object* owner)
{
    messager.addMessage(owner->getName() + " dies.", TCODColor::crimson);
    owner->setBlocking(false);
    owner->setCharacter(_deadSymbol);
    if(_deadName != "")
    {
        owner->setName(_deadName);
    }else
    {
        owner->setName(owner->getName() + " corpse");
    }
    owner->setFColor(TCODColor::crimson);
    game->sendToFront(owner);
}

int destructable::takeDamage(object* owner, object* source, damageTypes dmgType, int grossDamage, bool useMessage)
{
    static int tempDamage = 0;
    std::string damageString;
    object* tempObject;
    //compute the damaage done, check abilities, and see if the object taking damage dies.

    //check for abilities that trigger on being attacked;
    if(owner->getEquip())
    {
        tempObject = NULL;
        for(unsigned i = 0; i < owner->getEquip()->getSize(); i++)
        {
            tempObject = owner->getEquip()->getItem(i);
            if(tempObject)
            {
                if(tempObject->getPickable()->_wearable->_onBAttack)
                {
                    tempObject->getPickable()->_wearable->_onBAttack->apply->applyTo(source, owner);
                }
            }
        }
    }
    if(owner->getTraits())
    {
        if(owner->getTraits()->_onBAttack)
        {
            owner->getTraits()->_onBAttack->apply->applyTo(source, owner);
        }
    }
    //compute the damage
    tempDamage = grossDamage;
    //check to see if we even have to compute damage.
    tempDamage = calcDamage(tempDamage, dmgType);
    //check for abilities that trigger on taking damage
    if(owner->getEquip())
    {
        tempObject = NULL;
        for(unsigned i = 0; i < owner->getEquip()->getSize(); i++)
        {
            tempObject = owner->getEquip()->getItem(i);
            if(tempObject)
            {
                if(tempObject->getPickable()->_wearable->_onBHit)
                {
                    tempObject->getPickable()->_wearable->_onBHit->apply->applyTo(source, owner);
                }
            }
        }
    }
    if(owner->getTraits())
    {
        if(owner->getTraits()->_onBHit)
        {
            owner->getTraits()->_onBHit->apply->applyTo(source, owner);
        }
    }
    //check to see if defenses reduced damage to 0
    if(tempDamage <= 0 && dmgType != DMGTRUE)
    {
        if(useMessage)
            messager.addMessage(source->getName() + "'s attack does nothing to the " + owner->getName() + "!", TCODColor::yellow);
        return 0;
    }
    damageString = static_cast<std::ostringstream*>( &(std::ostringstream() << tempDamage) )->str();
    if(useMessage)
        messager.addMessage(source->getName() + " attacks " + owner->getName() + " for " + damageString + " damage!",
                        TCODColor::white);
    //check to see if damage is lethal
    if( (_currentHP - tempDamage) < 0 )
    {
        //oh snap, shiz is lethal.
        ;//check for abilities that trigger on death.
        _currentHP = -1;

        game->gainXP(owner->getDestroyable()->getXP());

        die(owner);
        return tempDamage;
    }else
    {
        if(_currentHP - tempDamage > int(_maxHp) )
        {
            tempDamage = _maxHp - _currentHP;
        }
        _currentHP -= tempDamage;
        return tempDamage;
    }
}

int destructable::heal(object* owner, unsigned amount)
{
    bool healed = true;
    if(_currentHP == int(_maxHp))
    {
        healed = false;
        return 0;
    }else if(_currentHP + amount > _maxHp)
    {
        amount = _maxHp - _currentHP;
        _currentHP = _maxHp;
        return amount;
    }else
    {
        _currentHP += amount;
        return amount;
    }
    return healed;
}

void playerDestructable::die(object* owner)
{
    //aight, player died. Time to end this game.
    destructable::die(owner);
}

playerDestructable::playerDestructable(unsigned maxHp, int physDef, int menDef, int xp, int dodge, int deadChar, std::string deadName) :
    destructable(maxHp, physDef, menDef, dodge, xp, deadChar, deadName)
{
    ;
}

void genericMonsterDestructable::die(object* owner)
{
    destructable::die(owner);
    //need to check for any abilities that trigger on death.
    ;

    //Woo, monster dead, remove the ability for it to die!
    owner->addDestructable();
    owner->addAI();
    //call the rest of this mamajamma

}

genericMonsterDestructable::genericMonsterDestructable(unsigned maxHp, int physDef, int menDef, int xp, int dodge, int deadChar, std::string deadName) :
    destructable(maxHp, physDef, menDef, xp, dodge, deadChar, deadName)
{
    ;
}

int destructable::changeHP(int cValue, object* owner)
{
    ;
}

int destructable::calcDamage(int amount, damageTypes type)
{
    if(amount == 0)
    {
        return 0;
    }
    switch(type)
    {
        case DMGPHYSICAL:
        {
            amount -= _physicalDef;
            break;
        }
        case DMGMENTAL:
        {
            amount -= _mentalDef;
            break;
        }
        case DMGSTATUS:
        case DMGTRUE:
        {
            break;
        }
    }
    return amount;
}
