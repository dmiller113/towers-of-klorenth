//Towers of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//container.cpp

//Includes
#include "container.h"

//Defines


//Globals


//Enums


//Classes


//Functions

container::~container()
{
    //dtor
    for(unsigned i = 0; i < _itemList.size(); i++)
    {
        if(_itemList[i] != NULL)
            delete _itemList[i];
    }
}

bool container::addItem(object* item)
{
    if( (_itemList.size() + 1) > _maxSize )
    {
        //too big, no room. OUT
        return false;
    }
    _itemList.push_back(item);
    return true;
}

object* container::getItem(unsigned pos)
{
    //check to see if pos is too big
    if(pos >= _maxSize || pos >= _itemList.size())
    {
        //pos too big. Return nothing.
        return NULL;
    }
    return _itemList[pos];
}

bool container::deleteItem(unsigned pos)
{
    object* tempObject = NULL;
    //check to see if pos is too big
    if(pos >= _maxSize)
    {
        //pos too big. Return nothing.
        return false;
    }
    tempObject = _itemList[pos];
    _itemList.erase(_itemList.begin() + pos);
    delete tempObject;
    return true;
}

bool container::deleteItem(object* item)
{
    unsigned pos = _itemList.size() + 1;
    //see if this thing in the container
    for(unsigned i = 0; i < _itemList.size(); i++)
    {
        if(_itemList[i] == item)
        {
            pos = i;
            break;
        }
    }
    if(pos == _itemList.size() + 1)
    {
        return false;
    }
    _itemList.erase(_itemList.begin() + pos);
    delete item;
    return true;

}

bool container::removeItem(unsigned pos)
{
    //check to see if pos is too big
    if(pos >= _maxSize)
    {
        //pos too big. Return nothing.
        return false;
    }
    _itemList.erase(_itemList.begin() + pos);
    return true;
}

bool container::removeItem(object* item)
{
    unsigned pos = _itemList.size() + 1;
    //see if this thing in the container
    for(unsigned i = 0; i < _itemList.size(); i++)
    {
        if(_itemList[i] == item)
        {
            pos = i;
            break;
        }
    }
    if(pos == _itemList.size() + 1)
    {
        return false;
    }
    _itemList.erase(_itemList.begin() + pos);
    return true;
}
