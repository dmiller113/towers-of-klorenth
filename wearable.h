#ifndef WEARABLE_H
#define WEARABLE_H

//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, (Year)
//wearable.h

//Includes
#include "libtcod.hpp"
#include "defines.h"
#include "effect.h"
#include "object.h"
#include "TargetSelector.h"

//Defines


//Globals


//Enums


//Classes

class effect;
class TargetSelector;

struct ability
{
   TargetSelector* selector;
   effect* apply;
   float range;
   std::string type;
   ability();
   ability(ability*);
   ~ability();
};


class wearable
{
public:
    equipLocations _location;
    int _patk;
    int _pdef;
    int _matk;
    int _mdef;
    int _dodge;
    ability* _onHit;
    ability* _onAttack;
    ability* _onBAttack;
    ability* _onBHit;
    ability* _onEquip;
    ability* _onRemove;
    wearable(equipLocations location, int physAtk = 0, int physDef = 0, int menAtk = 0, int menDef = 0, int dodgec = 0,
             ability* hit = NULL, ability* attack = NULL, ability* bAttack = NULL, ability* bHit = NULL, ability* equip = NULL,
             ability* onRemove = NULL);
    ~wearable();
    bool wear(object* owner, object* equiper);
    bool unwear(object* owner, object* equiper);
protected:
private:
};

#endif // WEARABLE_H
