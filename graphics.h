//Towers of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//graphics.h


#ifndef GRAPHICS_H
#define GRAPHICS_H
//Includes
#include "libtcod.hpp"
#include "object.h"
#include "map.h"
#include <string>
#include <vector>
#include "messageQueue.h"

//Defines


//Globals


//Enums


//Classes

class graphics
{
    TCODConsole* _mainScreen;
    TCODConsole* _messages;
    TCODConsole* _UI;
    TCODMap*     _fov;
    unsigned    _viewDistance;
    bool _fovDirty;
public:
    graphics();
    ~graphics();
    void drawPopupWindow(const std::string& title, const std::string& message, unsigned sizeX, unsigned sizeY, bool waitForKey = true);
    unsigned drawSelectionWindow(const std::string& title, std::vector<std::string> choices);
    void render(object& player, map& currentMap, std::vector<object*> objList);
    void drawMap(map& currentMap);
    void drawPlayer(object& player);
    void drawObjects(std::vector<object*> objList);
    void drawLines( void );
    void drawMessages( void );
    void drawUI( object& player );
    void remakeFOV(map& currentMap);
    void computeFOV( object* player );
    void setFoVDirty( void );
    void setViewDistance(int amount) {_viewDistance = amount;}
    TCODMap* getFov(void) { return _fov; }
};

extern graphics graphicEngine;
#endif // GRAPHICS_H
