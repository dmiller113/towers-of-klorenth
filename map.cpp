//Towers of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//map.cpp

//Includes

#include "map.h"
#include "object.h"
#include "gameEngine.h"
#include <iostream>


//Defines


//Globals
static const int ROOM_MAX_SIZE = 10;
static const int ROOM_MIN_SIZE = 6;

spawnChance monChances[MAXMONSTERDEFS] =
    { {30, 45, "Arcanist", 10, .5}, {45, 70, "Baron", 0, .25}, {5, 40, "Cultist", 3, .75}, {20, 50, "Demolisher", 5, .1}, {20, 50, "Elemental", 7, .2},
    {0, 20, "Fox", 5, .5}, {20, 30, "Gremlin", 0, .5}, {5, 20, "Hound", 5, .5}, {0, 15, "Ichor", 0, 1}, {5, 75, "Jack o' Lanturn", 1, .04},
    {30, 50, "Killer", 1, .25}, {15, 45, "Lemure", 5, .5}, {25, 55, "Mastif", 5, .5}, {45, 130, "Nightmare", 2, .1}, {40, 80, "Ogre", 5, .25},
    {30, 70, "Presence", 1, .5}, {60, 80, "Quarm", 5, .1}, {60, 110, "Ripper", 1, .25}, {20, 60, "Serpent", 3, .15}, {60, 150, "Terror", 1, .05},
    {0, 0, "Avatar", 0, 0} };

spawnChance itemChances[MAXITEMDEFS] =
    { {0, 30, "Weak Health Potion", 10, -.25}, {25, 60, "Health Potion", 17, -.25}, {50, 150, "Strong Health Potion", 10, 0}, {0, 60, "Poisoned Potion", 3, 0},
      {60, 90, "Strong Poison Potion", 3, 0}, {90, 150, "Toxic Potion", 3, 0}, {0, 50, "Lightning Scroll", 3, .1}, {35, 75, "Gale Scroll", 3, .2},
      {20, 90, "Firebolt Scroll", 1, .15}, {0, 40, "Brass Lanturn", 12, -.25}, {0, 30, "Iron Sword", 2, .2}, {5, 45, "Blood Sword", 1, .1},
      {20, 60, "Steel Sword", -2, .2}, {25, 70, "Vampiric Sword", -2, .1}, {0, 30, "Leather Armor", 5, .075}, {20, 70, "Plate Armor", -2, .125},
      {30, 75, "Barbed Armor", 2, .075}, {40, 90, "Shock Sword", -5, .125}, {0, 150, "Explosion Potion", 3, 0}, {50, 130, "Blizzard Scroll", 3, .025} };




static spawnChart SPAWNS(monChances, itemChances);
//Enums


//Classes


spawnChance::spawnChance() :
    minValue(0), maxValue(0), name(""), baseChance(0), valueScale(0)
{
    ;
}

spawnChance::spawnChance(unsigned mV, unsigned maV, std::string na, unsigned bC, float vS) :
    minValue(mV), maxValue(maV), name(na), baseChance(bC), valueScale(vS)
{
    ;
}
unsigned spawnChance::getChance(int dLevel)
{
    dLevel = dLevel + game->getPLevel()*5;
    if(dLevel >= minValue && dLevel < maxValue)
        return baseChance + (valueScale*dLevel);
    else
        return 0;
}


spawnChart::spawnChart(spawnChance monsterSpawns[MAXMONSTERDEFS], spawnChance itemSpawns[MAXITEMDEFS])
{
    for(int i = 0; i < MAXMONSTERDEFS; i++)
    {
        _monsterChances.push_back(monsterSpawns[i]);
    }
    for(int i = 0; i < MAXITEMDEFS; i++)
    {
        _itemChances.push_back(itemSpawns[i]);
    }
}

std::string spawnChart::getMonsterSpawn(int rand, int dlevel)
{
    int totalChance = getMaxChance(dlevel);
    int entry = -1;
    do
    {
        entry += 1;
        rand -= _monsterChances[entry].getChance(dlevel);

    }while(entry < _monsterChances.size() && rand > 0);
    return _monsterChances[entry].name;
}

std::string spawnChart::getItemSpawn(int rand, int dlevel)
{
    int totalChance = getMaxChance(dlevel, false);
    int entry = -1;
    do
    {
        entry++;
        rand -= _itemChances[entry].getChance(dlevel);
    }while(entry < _itemChances.size() && rand > 0);
    return _itemChances[entry].name;
}

int spawnChart::getMaxChance(int dlevel, bool monster)
{
    int totalChance = 0;
    if(monster)
    {
        for(unsigned i = 0; i < _monsterChances.size(); i++)
        {
            totalChance += _monsterChances[i].getChance(dlevel);
        }
    }else
    {
        for(unsigned i = 0; i < _itemChances.size(); i++)
        {
            totalChance += _itemChances[i].getChance(dlevel);
        }
    }
    return totalChance;
}

class BspListener : public ITCODBspCallback
{
    map &_map; // a map to dig
    int roomNum; // room number
    int lastx,lasty; // center of the last room
    bool isStairs;
public:
    BspListener(map &aMap) : _map(aMap), roomNum(0), isStairs(false) {;}
    bool visitNode(TCODBsp *node, void *userData)
    {
        if ( node->isLeaf() )
        {
            int x,y,w,h;

            // dig a room
            //w=_map.rand->getInt(ROOM_MIN_SIZE, node->w-2);
            w = node->w-2;
            //h=_map.rand->getInt(ROOM_MIN_SIZE, node->h-2);
            h = node->h-2;
            x =_map.rand->getInt(node->x+1, node->x+node->w-w-1);
            y =_map.rand->getInt(node->y+1, node->y+node->h-h-1);
            _map.createRoom(roomNum == 0, x, y, x+w-1, y+h-1, isStairs);
            if ( roomNum != 0 )
            {
                // dig a corridor from last room
                _map.dig(lastx,lasty,x+w/2,lasty);
                _map.dig(x+w/2,lasty,x+w/2,y+h/2);
            }
            lastx=x+w/2;
            lasty=y+h/2;
            roomNum++;
        }
        return true;
    }
};

//Functions

map::map()
{
    //debug map constructor
    //std::cout<<"in map()"<<std::endl;
    for(unsigned x = 0; x < MAPMAXX; x++)
    {
        for(unsigned y = 0; y < MAPMAXY; y++)
        {
            _map[x][y]._symbol = '#';
            _map[x][y]._backColor = TCODColor::black;
            _map[x][y]._foreColor = TCODColor::grey;
            _map[x][y].isTransparent = false;
            _map[x][y].isWalkable = false;
            _map[x][y].isExplored = false;
        }
    }

    for(unsigned x = 5; x < 35; x++)
    {
        for(unsigned y = 15; y < 35; y++)
        {
            _map[x][y]._symbol = '.';
            _map[x][y]._backColor = TCODColor::black;
            _map[x][y]._foreColor = TCODColor::lightGrey;
            _map[x][y].isTransparent = true;
            _map[x][y].isWalkable = true;
            _map[x][y].isExplored = false;
        }
    }
    _map[15][20]._symbol = '#';
    _map[15][20]._backColor = TCODColor::black;
    _map[15][20]._foreColor = TCODColor::grey;
    _map[15][20].isTransparent = false;
    _map[15][20].isWalkable = false;
    _map[15][20].isExplored = false;
}

map::map(int seed)
{
    rand = new TCODRandom(seed);
    for(unsigned x = 0; x < MAPMAXX; x++)
    {
        for(unsigned y = 0; y < MAPMAXY; y++)
        {
            _map[x][y]._symbol = '#';
            _map[x][y]._backColor = TCODColor::black;
            _map[x][y]._foreColor = TCODColor::grey;
            _map[x][y].isTransparent = false;
            _map[x][y].isWalkable = false;
            _map[x][y].isExplored = false;
        }
    }
}

map::~map()
{
    //dtor
    delete rand;
}

bool map::populateMap()
{
    TCODBsp bsp(0,0,MAPMAXX, MAPMAXY);
    bsp.splitRecursive(rand,8,ROOM_MAX_SIZE,ROOM_MAX_SIZE,1.5f,1.5f);
    BspListener listener(*this);
    bsp.traverseInvertedLevelOrder(&listener,NULL);
}

tile* map::getTile(unsigned x, unsigned y)
{
    if(x < MAPMAXX && y < MAPMAXY)
    {
        return &(_map[x][y]);
    }
    return &(_map[0][0]);
}

bool map::isWalkable(int x, int y)
{
    int tempEPos[2];
    object* tempObject = NULL;
    bool canWalk = true;
    if(!_map[x][y].isWalkable)
    {
        canWalk = false;
    }else
    {
        for(unsigned i = 0; i < game->getList().size(); i++)
        {
            tempObject = game->getObject(i);
            tempObject->getPos(tempEPos);
            if(tempEPos[0] == x && tempEPos[1] == y)
            {   //this object is in the way!
                //check to see if this object blocks pathing!
                if( tempObject->isBlocking() || tempObject->getDestroyable() != NULL)
                {
                    //Oh snap, this object blocks pathing. You bumped yo!
                    canWalk = false;
                    break;
                }
            }
        }
    }
    return canWalk;
}

void map::dig(int x1, int y1, int x2, int y2)
{
    if ( x2 < x1 ) {
        int tmp=x2;
        x2=x1;
        x1=tmp;
    }
    if ( y2 < y1 ) {
        int tmp=y2;
        y2=y1;
        y1=tmp;
    }
    for (int tilex=x1; tilex <= x2; tilex++) {
        for (int tiley=y1; tiley <= y2; tiley++) {
            _map[tilex][tiley].isWalkable = true;
            _map[tilex][tiley].isTransparent = true;
            if(_map[tilex][tiley]._symbol != '>')
                _map[tilex][tiley]._symbol = '.';
            _map[tilex][tiley]._backColor = TCODColor::black;
            _map[tilex][tiley]._foreColor = TCODColor::lightGrey;
        }
    }
}

void map::createRoom(bool first, int x1, int y1, int x2, int y2, bool& isStairs)
{
    dig (x1,y1,x2,y2);
    static int sChance = 1;
    int monsterChance = 0;
    int itemChance = 0;
    std::string spawnName;
    object* tempObject;
    int tempX1, tempY1, tempX2, tempY2;
    if(first)
    {
        int tPos[2] = {(x1+x2)/2, (y1+y2)/2};
        game->getPlayer()->setPos(tPos);
    }else
    {
        itemChance = rand->getInt(0,99);
        tempX1 = x1>x2?x2:x1;
        tempY1 = y1>y2?y2:y1;
        tempX2 = x1<x2?x2:x1;
        tempY2 = y1<y2?y2:y1;
        tempX1 = rand->getInt(tempX1 + 2, tempX2 - 2);
        tempY1 = rand->getInt(tempY1 + 2, tempY2 - 2);
        monsterChance = rand->getInt(1, SPAWNS.getMaxChance(game->getLevel()) );
        spawnName = SPAWNS.getMonsterSpawn(monsterChance, game->getLevel());
        tempObject = game->mFactory.createMonster(spawnName, tempX1, tempY1);
        if(tempObject)
            {
                game->getList().push_back(tempObject);
            }
        if(itemChance < game->getLevel()*4)
        {
            itemChance = rand->getInt(1, SPAWNS.getMaxChance(game->getLevel(), false) );
            spawnName = SPAWNS.getItemSpawn(itemChance, game->getLevel());
            tempObject = game->iFactory.makeItem(tempX1, tempY1, spawnName);
            if(tempObject)
            {
                game->getList().push_back(tempObject);
                game->sendToFront(tempObject);
            }
        }
    }
    if(!isStairs)
    {
        int stairChance = rand->getInt(0,99);
        if(stairChance < (sChance*10))
        {
            sChance = 1;
            tempX1 = x1>x2?x2:x1;
            tempY1 = y1>y2?y2:y1;
            tempX2 = x1<x2?x2:x1;
            tempY2 = y1<y2?y2:y1;
            tempX1 = rand->getInt(tempX1 + 1, tempX2 - 1);
            tempY1 = rand->getInt(tempY1 + 1, tempY2 - 1);
            _map[tempX1][tempY1]._symbol = '>';
            isStairs = true;
        }else
        {
            sChance++;
        }
    }
}
