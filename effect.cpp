//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//effect.h

//Includes


//Defines


//Globals


//Enums


//Classes


//functions

#include "effect.h"
#include <sstream>
#include "messageQueue.h"
#include "gameEngine.h"
#include "graphics.h"

effect::effect()
{
    //ctor
}

effect::~effect()
{
    //dtor
}

changeHealthEffect::changeHealthEffect(int amount, damageTypes type, std::string report) :
    _amount(amount), _type(type), _message(report)
{
    ;
}

bool changeHealthEffect::applyTo(object* target, object* source)
{
    int damageDone = 0;
    std::string damageString;
    std::string oldName;
    TCODColor messageColor = _amount > 0?TCODColor::white:TCODColor::lime;

    //amounts over 0 are damage. Call the damage function.
    //heals need to have DMGTRUE or they'll be reduced by the targets defenses.
    if(target->getDestroyable())
    {
        oldName = target->getName();
        if(_amount > 0)
        {
            damageDone = abs(target->getDestroyable()->takeDamage(target, source, _type, _amount, false));
        }else
        {
            damageDone = target->getDestroyable()->heal(target, -1*_amount);
        }
        //All messages regarding health effects need to end in ...for
        damageString = static_cast<std::ostringstream*>( &(std::ostringstream() << damageDone) )->str();
        if(damageDone != 0)
        {
            messager.addMessage(oldName + _message + damageString + " health!", messageColor);
            return true;
        }
    }
    return false;
}

changeHealthEffect::changeHealthEffect( changeHealthEffect* old )
{
    _amount = old->_amount;
    _type = old->_type;
    _message = old->_message;
}

changeHealthEffect* changeHealthEffect::getCopy( void )
{
    changeHealthEffect* temp = new changeHealthEffect(_amount, _type, _message);
    return temp;
}

changeLightRadiusEffect::changeLightRadiusEffect(int amount, std::string report)
{
    //This effect won't work on monsters. Always effects the player.
    _amount =amount;
    _message = report;
}

bool changeLightRadiusEffect::applyTo(object* target, object* source)
{
    //this effect increases/decreases the sight radius of the playr by amount. Doesn't work on monsters.
    TCODColor messageColor = _amount > 0?TCODColor::lime:TCODColor::yellow;
    _amount = abs(_amount);
    if(target != game->getPlayer())
        return false; //Only works if the player is the target.
    graphicEngine.setViewDistance(_amount);
    std::string tempString = source->getName() + _message;
    messager.addMessage(tempString.c_str(), messageColor);
    graphicEngine.computeFOV(game->getPlayer());
    return true;
}

changeLightRadiusEffect* changeLightRadiusEffect::getCopy( void )
{
    changeLightRadiusEffect* temp = new changeLightRadiusEffect(_amount, _message);
    return temp;
}
