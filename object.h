//Towers of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//object.h

#ifndef OBJECT_H
#define OBJECT_H

 //Includes
#include "libtcod.hpp"
#include "AI.h"
#include "destructable.h"
#include "damager.h"
#include <string>
#include <vector>
#include <fstream>
#include "container.h"
#include "pickable.h"
#include "wearable.h"
#include "specialTraits.h"

//Defines


//Globals
const unsigned MAXIDABLES = 15;

//Enums


//Classes
class AI;
class destructable;
class damager;
class container;
class pickable;
class effect;
class TargetSelector;
class specialTraits;
struct ability;
struct message;


class object
{
    std::string _name;
    std::string _description;
    int _xPos;
    int _yPos;
    int _character;
    TCODColor _backColor;
    TCODColor _foreColor;
    bool _isBlocking;
    destructable* _destroyable;
    damager* _damager;
    container* _inventory;
    container* _equipped;
    pickable* _item;
    AI* _AI;
    specialTraits* _traits;
public:
    object(std::string name, int x, int y, int character, const TCODColor& back, const TCODColor& fore, bool blocks, std::string desc = "");
    ~object();
    void draw(TCODConsole* con);
    void addAI(AI* newAI = NULL);
    AI* getAI(void) const  {return _AI;}
    void addDamager(damager* newDamager = NULL);
    damager* getDamager(void) const  {return _damager;}
    void addDestructable(destructable* newDestroyable = NULL);
    destructable* getDestroyable(void) const  {return _destroyable;}
    void addEquip(container* newEquip = NULL);
    container*  getEquip(void) {return _equipped;}
    void getPos(int Pos[2]);
    void movePos(int cX = 0, int cY = 0);
    void setPos(int pos[2]);
    void setName(std::string name) {_name = name;}
    std::string getName(void) {return _name;}
    bool isBlocking( void ) {return _isBlocking;}
    void setBlocking( bool isBlocking ) {_isBlocking = isBlocking;}
    void setCharacter( int newChar ) {_character = newChar;}
    void setFColor(TCODColor newColor) {_foreColor = newColor;}
    void setBColor(TCODColor newColor) {_backColor = newColor;}
    void addInventory(container* inventory = NULL);
    container* getContainer(void)   {return _inventory;}
    void addPickable(pickable* pick = NULL);
    pickable* getPickable(void)   {return _item;}
    void setDescription(std::string desc) {_description = desc;}
    std::string getDescription(void) {return _description;}
    void addTraits(specialTraits* trait = NULL);
    specialTraits* getTraits(void)   {return _traits;}

};


class monsterFactory
{
    struct monsterTraits
    {
        int symbol;
        std::string name;
        TCODColor backColor;
        TCODColor foreColor;
        std::string description;
        bool blocks;
        int strength;
        int dex;
        int will;
        int maxHP;
        int dodge;
        int pdef;
        int mdef;
        int patk;
        int matk;
        int sightRadius;
        int chaseTurns;
        int energyCharge;
        int xp;

    };
    std::vector<monsterTraits> monsterDefs;
public:
    monsterFactory(std::string filePath = "monsters.txt");
    //~monsterFactory();
    object* createMonster(int symbol, int xPos, int yPos);
    object* createMonster(std::string name, int xPos, int yPos);
    object* createMonster(int symbol, int xPos, int yPos,
        int strength, int dex, int will, int maxHP, int dodge, int pdef,
        int mdef, int patk, int matk, int sightRadius, int chaseTurns, int energyCharge);

};

struct idEntry
{
    bool _isID;
    std::string _realName;
    std::string _fakeName;
    std::string _type;
    TCODColor _randomColor;
};

class itemFactory
{
    struct itemTraits
    {
        int symbol;
        std::string realName;
        std::string fakeName;
        std::string description;
        TCODColor foreColor;
        TCODColor backColor;
        bool isID;
        bool isEquip;
        bool isPerm;
        int dodge;
        int pdef;
        int mdef;
        int patk;
        int matk;
        int sightRadius;
        int charges;
        equipLocations location;
        ability* onUse;
        ability* onHit;
        ability* onAttack;
        ability* onBAttack;
        ability* onBHit;
        ability* onEquip;
        ability* onRemove;
        //message* forUse;
        //message* forHit;
        //message* forAttack;
        //message* forBAttack;
        //message* forBHit;
        //message* forEquip;
        //message* forRemove;

    };
    std::vector<itemTraits> itemDefs;
    std::vector<idEntry> idArray;
public:
    itemFactory(std::string filePath = "items.txt");
    bool randomizeIDs( void );
    bool setIDs(unsigned pos, bool isID, TCODColor randomColor);
    object* makeItem(int x, int y, std::string realName, int charges = -1);

};
#endif // OBJECT_H
