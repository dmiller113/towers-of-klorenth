//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, 2013
//TargetSelector.h

//Includes
#include "gameEngine.h"
#include "messageQueue.h"
//Defines


//Globals


//Enums


//Classes


//Functions

#include "TargetSelector.h"

TargetSelector::TargetSelector( SelectorType type, float range ) :
    _type(type), _range(range)
{
    //ctor
    ;
}

TargetSelector::~TargetSelector()
{
    //dtor
}

void TargetSelector::selectTargets(object* source, std::vector<object*>& listOfObjects )
{
    int tempPos[2];
    source->getPos(tempPos);
    switch(_type) {
        case CLOSEST_MONSTER :
        {
            object *closestMonster = game->getClosestMonster(tempPos[0], tempPos[1], _range);
            if ( closestMonster )
            {
                listOfObjects.push_back(closestMonster);
            }
            break;
        }
        case SELECTED_MONSTER :
        {
            int x,y;
            messager.addMessage("Select an enemy, or press escape to cancel.", TCODColor::cyan);
            if ( game->pickATile(x, y, _range))
            {
                object *actor = game->getObject(x, y);
                if ( actor ) {
                    listOfObjects.push_back(actor);
                }
            }
            break;
        }
        case PBAOE :
        {
            int tempOPos[2];
            for (unsigned i = 0; i < game->getList().size(); i++)
            {
                object* actor = game->getList()[i];
                actor->getPos(tempOPos);

                if ( actor != source && actor->getDestroyable() && !actor->getDestroyable()->isDead()
                    && getDistance(tempPos[0], tempPos[1],tempOPos[0], tempOPos[1]) <= _range)
                {
                    listOfObjects.push_back(actor);
                }
            }
            break;
        }
        case SELECTED_TILE :
        {
            int x,y;
            int tempOPos[2];
            messager.addMessage("Select a tile, or press escape to cancel.", TCODColor::cyan);
            if ( game->pickATile(x,y))
            {
                for (unsigned i = 0; i < game->getList().size(); i++)
                {
                    object* actor = game->getList()[i];
                    actor->getPos(tempOPos);
                    if ( actor->getDestroyable() && !actor->getDestroyable()->isDead()
                        && getDistance(tempPos[0], tempPos[1],tempOPos[0], tempOPos[1]) <= _range)
                    {
                        listOfObjects.push_back(actor);
                    }
                }
            }
            break;
        }

    }

}
