//Tower of Klorenth, 7drl
//Writen designed and planned by Daniel Miller, (Year)
//gameEngine.h


#ifndef GAMEENGINE_H
#define GAMEENGINE_H

//Includes
#include "libtcod.hpp"
#include "defines.h"
#include "map.h"
#include "object.h"
#include "AI.h"
#include <vector>

//Defines


//Globals


//Enums


//Classes


//Functions


class gameEngine
{
    map* _currentMap;

    object* _player;
    unsigned _playerXP;
    unsigned _playerLevel;
    std::vector<object*> _objectList;
    unsigned _dlevel;
    int _seed;
public:
    monsterFactory mFactory;
    itemFactory iFactory;
    gameEngine(int seed = 0);
    ~gameEngine();
    void init(int seed = 0);
    void tick(gameState& gs);
    object* getPlayer( void );
    map* getMap( void );
    void createMap();
    object* getObject(unsigned pos);
    object* getObject(int x, int y);
    void removeObject(unsigned pos, bool isDeleted = true);
    void removeObject(object* item, bool isDeleted = true);
    void sendToFront(unsigned pos);
    void sendToFront(object* target);
    std::vector<object*>& getList(void) {return _objectList;}
    unsigned getLevel(void) {return _dlevel;}
    unsigned getPLevel(void) {return _playerLevel;}
    unsigned getPEXP(void) {return _playerXP;}
    void nextLevel( void );
    int gainXP(int amount);
    int returnNextLevel(void);
    void checkLevel(void);
    object* getClosestMonster(int x, int y, float range);
    bool pickATile(int& x, int& y, float range = 0.0);
};

extern gameEngine* game;

#endif // GAMEENGINE_H
